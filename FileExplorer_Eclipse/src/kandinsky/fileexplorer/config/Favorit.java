package kandinsky.fileexplorer.config;

import java.io.File;

/**
 * Hilfsklasse um Daten eines Favoriten zu speichern
 * 
 * @author Jan
 *
 */
public class Favorit {
	private String bezeichnung;
	private File file;
	
	public Favorit(File file, String bezeichnung){
		this.bezeichnung = bezeichnung;
		this.file = file;
	}
	
	public Favorit(String pfad, String bezeichnung){
		this.bezeichnung = bezeichnung;
		this.file = new File(pfad);
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}
	
	
	
}
