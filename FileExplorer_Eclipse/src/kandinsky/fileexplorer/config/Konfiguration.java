package kandinsky.fileexplorer.config;

import java.awt.Dimension;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Properties;
import java.util.prefs.Preferences;

public class Konfiguration {

	private final static String DEFAULT_FILE = "./default.ini";
	private final static String OPTION_FILE = "./option.ini";


	// TODO Variablen f�r die Standarteinstellung standart h�hen f�r Fenster und
	// Panels.
	// TODO sichtbarkeits/Reinfole-variablen f�r ver�nderbaren Elemente der Gui
	
	private  boolean zeigeVersteckteDateien = false;
	private  boolean zeigeRechtesOrdnerFenster = true; //muss immer true !!(Kein Lust auf nochmehr sonder faeller in der baueMittleresFenster() im Mainframe
	private  boolean zeigeLinkesOrdnerFenster = false; //standart false
	private  boolean zeigeBaumFenster = true;	
	private  boolean zeigePfadLeisteRechts = true;
	private  boolean zeigePfadLeisteLinks = true;
	private  boolean zeigeToolbar = true;	
	private  boolean isAktiveSuche = false;//TODO kann noch nicht eingestellt werden
	
	private  Dimension minFenstergroesse = new Dimension(100,300);
	private  Dimension optFenstergroesse  = new Dimension(900, 600);
	private  Dimension minBaumFenster = new Dimension(70, 23);

	private  int breiteFensterLinks = 250;
	private  int breiteBaum = 300;
	private  int trennerBreite = 2;
	
	
	private  String inizialPfadLinks = "C:/";
	private  String inizialPfadRechts = "C:/";
	private  String programmName = "Kandinsky File Explorer";
	private  String NeuerOrdnerName = "Neuer Ordner";
	private  String neueDateiName = "Neue Datei";
	
	private  byte selectedViewFensterRechts = 1;//default wert f�r VIEW_LIST=1 
	private  byte selectedViewFensterLinks = 1;//default wert f�r VIEW_LIST=1 

	//private Preferences prefs;
/*
	public Konfiguration() {
		//System.out.println(new File(".").getAbsolutePath());
		System.out.println(new File("default.ini").getAbsolutePath());
		Properties prefs = new Properties();
		boolean erfolg;
		
		//do {
			try {
				prefs.load(new FileInputStream(OPTION_FILE));
				erfolg = true;
				if(erfolg)
					System.out.println("true");
				else
					System.out.println("false");
			} catch (FileNotFoundException e) {
				erfolg = false;
				if(erfolg)
					System.out.println("true");
				else
					System.out.println("false");
				generateStandardFile();
			} catch (IOException e) {
				erfolg = false;
			} catch (Exception e) {
				erfolg = false;
			}
		//} while (!erfolg);
		
	}
	*/

	public void generateStandardFile() {
		// TODO: Eine Standard Datei erzeugen mit Defaultwerten
	}

	public void ladeOptionDatei() {
		// TODO
	}

	public void speicherOptionDatei() {
		// TODO
	}

	public void ladeDefaultDatei() {
		// TODO
	}

	// Vielle vieeele getter und setter---------------------------------------------

	public boolean isZeigeVersteckeDateien() {
		return zeigeVersteckteDateien;
	}

	public void setZeigeVersteckeDateien(boolean zeigeVersteckteDateien) {
		this.zeigeVersteckteDateien = zeigeVersteckteDateien;
	}
	public byte getSelectedView() {
		return selectedViewFensterRechts;
	}

	public void setSelectedView(byte selectedView) {
		this.selectedViewFensterRechts = selectedView;
	}

	public boolean isZeigeRechtesOrdnerFenster() {
		return zeigeRechtesOrdnerFenster;
	}

	public void setZeigeRechtesOrdnerFenster(boolean zeigeOrdnerFenster1) {
		this.zeigeRechtesOrdnerFenster = zeigeOrdnerFenster1;
	}

	public boolean isZeigeLinkesOrdnerFenster() {
		return zeigeLinkesOrdnerFenster;
	}

	public void setZeigeLinkesOrdnerFenster(boolean zeigeOrdnerFenster2) {
		this.zeigeLinkesOrdnerFenster = zeigeOrdnerFenster2;
	}

	public boolean isZeigeBaumFenster() {
		return zeigeBaumFenster;
	}

	public void setZeigeBaumFenster(boolean zeigeBaumFenster) {
		this.zeigeBaumFenster = zeigeBaumFenster;
	}

	public Dimension getMinFenstergroesse() {
		return minFenstergroesse;
	}

	public void setMinFenstergroesse(Dimension minFenstergroesse) {
		this.minFenstergroesse = minFenstergroesse;
	}

	public Dimension getOptFenstergroesse() {
		return optFenstergroesse;
	}

	public void setOptFenstergroesse(Dimension optFenstergroesse) {
		this.optFenstergroesse = optFenstergroesse;
	}


	public int getTrennerBreite() {
		return trennerBreite;
	}

	public void setTrennerBreite(int trennerBreite) {
		this.trennerBreite = trennerBreite;
	}

	public String getProgrammName() {
		return programmName;
	}

	public void setProgrammName(String programmName) {
		this.programmName = programmName;
	}

	public Dimension getMinBaumFenster() {
		return minBaumFenster;
	}

	public void setMinBaumFenster(Dimension minBaumFenster) {
		this.minBaumFenster = minBaumFenster;
	}

	public int getBreiteBaum() {
		return breiteBaum;
	}

	public void setBreiteBaum(int breiteBaum) {
		this.breiteBaum = breiteBaum;
	}

	public int getBreiteFensterLinks() {
		return breiteFensterLinks;
	}

	public void setBreiteFensterLinks(int breiteFensterLinks) {
		this.breiteFensterLinks = breiteFensterLinks;
	}

	public  byte getSelectedViewFensterLinks() {
		return selectedViewFensterLinks;
	}

	public  void setSelectedViewFensterLinks(byte selectedViewFensterLinks) {
		this.selectedViewFensterLinks = selectedViewFensterLinks;
	}

	public boolean isZeigeToolbar() {
		return zeigeToolbar;
	}

	public  void setZeigeToolbar(boolean zeigeToolbar) {
		this.zeigeToolbar = zeigeToolbar;
	}

	public  boolean isZeigePfadLeisteRechts() {
		return zeigePfadLeisteRechts;
	}

	public  void setZeigePfadLeisteRechts(boolean zeigePfadLeiste) {
		this.zeigePfadLeisteRechts = zeigePfadLeiste;
	}

	public String getNeuerOrdnerName() {
		return NeuerOrdnerName;
	}

	public void setNeuerOrdnerName(String neuerOrdnerName) {
		NeuerOrdnerName = neuerOrdnerName;
	}

	public String getInizialPfadLinks() {
		return inizialPfadLinks;
	}

	public void setInizialPfadLinks(String inizialPfad) {
		this.inizialPfadLinks = inizialPfad;
	}

	public boolean isZeigePfadLeisteLinks() {
		return zeigePfadLeisteLinks;
	}

	public void setZeigePfadLeisteLinks(boolean zeigePfadLeisteLinks) {
		this.zeigePfadLeisteLinks = zeigePfadLeisteLinks;
	}

	public  String getInizialPfadRechts() {
		return inizialPfadRechts;
	}

	public  void setInizialPfadRechts(String inizialPfadRechts) {
		this.inizialPfadRechts = inizialPfadRechts;
	}

	public boolean isAktiveSuche() {
		return isAktiveSuche;
	}

	public void setAktiveSuche(boolean isAktiveSuche) {
		this.isAktiveSuche = isAktiveSuche;
	}

	public String getNeueDateiName() {
		return neueDateiName;
	}
	
	
}
