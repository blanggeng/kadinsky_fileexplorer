package kandinsky.fileexplorer.config;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;


/**
 * Klasse die den Zugriff auf Favoriten erlaubt
 * 
 * @author Jan
 *
 */
public abstract class Favoriten {

	public static final String PATH_FAVORITEN_FILE = "favs.txt";
	private static final String SEPERATOR = ";";
	private static List<Favorit> favs;
	
	/**
	 * @return die Favoriten-Liste
	 */
	public static List<Favorit> getFavoriten(){
		check();
		return favs;
	}
	
	/**
	 * Laed die Favoriten-Datei
	 */
	public static void loadFavs(){
		favs = new LinkedList<>();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(PATH_FAVORITEN_FILE));
			String line = reader.readLine();
			while(line!=null){
				try{
					String[] splitted = line.split(SEPERATOR);
					favs.add(new Favorit(splitted[0], splitted[1]));
				} catch (Throwable e) {
					e.printStackTrace();
				}
				line = reader.readLine();
			}
			reader.close();
			callAllListeners();
		} catch (IOException e) {
		}
		
	}
	/**
	 * Entfernt einen Favoriten aus der Liste und speichert diese ab
	 * 
	 * @param f
	 */
	public static void removeFavorit(Favorit f){
		favs.remove(f);
		saveFavs();
		callAllListeners();
	}

	/**
	 *  speichert die Favoriten
	 */
	public static void saveFavs(){
		if(favs==null) return;
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(PATH_FAVORITEN_FILE));
			
			for(Favorit f: favs){
				writer.write(f.getFile().getAbsolutePath());
				writer.write(SEPERATOR);
				writer.write(f.getBezeichnung());
				writer.newLine();
			}
			
			writer.flush();
			writer.close();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
	}
	/**
	 * Fuegt einen Favoriten hinzu
	 * 
	 * @param f
	 * @param bezeichnung
	 */
	public static void addFavorit(File f, String bezeichnung){
		check();
		favs.add(new Favorit(f,bezeichnung));
		saveFavs();
		callAllListeners();
	}
	/**
	 * Prueft ob eine Datei ein Favorit ist
	 * 
	 * @param f
	 * @return
	 */
	public static boolean containsFile(File f){
		check();
		for(Favorit fav: favs){
			if(fav.getFile().equals(f)){
				return true;
			}
		}
		return false;
	}
	
	private static void check(){
		if(favs==null){
			loadFavs();
		}
	}
	
	
	// Observer
	private static LinkedList<IFavoritenListener> listeners = new LinkedList<>();
	/**
	 * Fuegt einen Listener hinzu
	 * 
	 * @param listener
	 */
	public static void addListener(IFavoritenListener listener){
		listeners.add(listener);
	}
	/**
	 * Entfernt einen Listener
	 * 
	 * @param listener
	 */
	public static void removeListener(IFavoritenListener listener){
		listeners.remove(listener);
	}
	private static void callAllListeners(){
		for(IFavoritenListener x: listeners){
			x.favsChange();
		}
	}
	/**
	 * Sorgt dafuer, dass alle Favoriten-Beobachter sich aktualisieren. => Umbennenung eines Favoriten
	 */
	public static void updateAllListeners(){
		callAllListeners();
	}
	
}
