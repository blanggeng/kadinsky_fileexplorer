package kandinsky.fileexplorer.config;

public class Preference<E> {
	private E value;
	private String identifier;
	
	public Preference(String identifier, E value){
		this.identifier = identifier;
		this.value = value;
	}
	
	public void setValue(E value){
		this.value = value;
	}
	
	public void setIdentifier(String identifier){
		this.identifier = identifier;
	}
	
	public E getValue(){
		return value;
	}
	
	public String getIdentifier(){
		return identifier;
	}
	
	public String toString(){
		return identifier + ", " + value.toString();
	}
	
	
}
