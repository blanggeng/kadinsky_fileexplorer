package kandinsky.fileexplorer.config;


/**
 * Informiert Beobachter ob sich die Favoritenliste geaendert hat
 * 
 * @author Jan
 *
 */
public interface IFavoritenListener{
	/**
	 * Favoriten haben sich geaendert
	 */
	public void favsChange();
}