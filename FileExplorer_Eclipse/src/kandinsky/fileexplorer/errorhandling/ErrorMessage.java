package kandinsky.fileexplorer.errorhandling;

import java.awt.Component;

import javax.swing.JOptionPane;

/**
 * Klasse zum Erzeugen und ausgeben von Fehlermeldungen
 * @author Giuseppe Patane
 *
 */
public class ErrorMessage {
	
	/**
	 * Konstanten f�r Fehlerlevel
	 * QUESTION
	 * FATAL_ERROR 
	 * ERROR
	 * WARNING
	 * MESSAGE
	 * INFO 	 
	 */
	public static final int QUESTION = 5;
	public static final int FATAL_ERROR = 4; 
	public static final int ERROR = 3;
	public static final int WARNING = 2;
	public static final int MESSAGE = 1;
	public static final int INFO = 0;
	public static final int YES = 0;
	public static final int NO = 1;
	public static final int ABBORT = 2;
	
	private int errorLevel;
	private Component parent;
	private String errorMessage;
	private int errorNumber = 0;
	private int result;
	
	/**
	 * Erzeugt eine Messagebox mit zus�tzlich einer Fehlernummer 
	 * @param parent			View in dem die Messagebox erzeugt wird
	 * @param errorMessage		String mit der Fehlermeldung/ Info
	 * @param errorLevel		Grad des Fehlers (siehe Konstanten f�r Errorlevel)
	 * @param errorNumber		Eindeutige Fehlernummer
	 */
	public ErrorMessage(Component parent, String errorMessage, int errorLevel, int errorNumber){
		this(parent, errorMessage, errorLevel);
		this.errorNumber = errorNumber;		
	}
	
	/**
	 * Erzeugt eine Messagebox f�r Fehlerausgabe 
	 * @param parent			View in dem die Messagebox erzeugt wird
	 * @param errorMessage		String mit der Fehlermeldung/ Info
	 * @param errorLevel		Grad des Fehlers (siehe Konstanten f�r Errorlevel)
	*/
	public ErrorMessage(Component parent, String errorMessage, int errorLevel){
		this.errorLevel = errorLevel;
		this.parent = parent;
		this.errorMessage = errorMessage;
		
		switch(errorLevel){
			case INFO:			createInfoDialogue();
								break;
			case MESSAGE:		createMessageDialogue();
								break;
			case WARNING:		createWarningDialogue();
								break;
			case ERROR:			createErrorDialogue();
								break;
			case FATAL_ERROR:	createFatalErrorDialogue();
								break;
			case QUESTION:		createQuestionDialogue();
								break;
			default:
		}
	}
	
	private void createQuestionDialogue(){
		result= JOptionPane.showConfirmDialog(parent, errorMessage, "Question", JOptionPane.YES_NO_OPTION);
	}
	
	private void createFatalErrorDialogue() {
		JOptionPane.showMessageDialog(parent, errorMessage, "Fatal Error", JOptionPane.ERROR_MESSAGE);
	}

	private void createMessageDialogue() {
		JOptionPane.showMessageDialog(parent, errorMessage, "Message", JOptionPane.INFORMATION_MESSAGE);
		
	}

	private void createInfoDialogue() {
		JOptionPane.showMessageDialog(parent, errorMessage, "Info", JOptionPane.INFORMATION_MESSAGE);
		
	}

	private void createWarningDialogue(){
		JOptionPane.showMessageDialog(parent, errorMessage, "Warnung", JOptionPane.WARNING_MESSAGE);
	}
	
	private void createErrorDialogue(){
		JOptionPane.showMessageDialog(parent, errorMessage, "Fehler", JOptionPane.ERROR_MESSAGE);
	}
	
	public int getResult(){
		return result;
	}
	
	public String toString(){
		return "Error " + errorNumber + " - " + errorMessage;
	}

}
