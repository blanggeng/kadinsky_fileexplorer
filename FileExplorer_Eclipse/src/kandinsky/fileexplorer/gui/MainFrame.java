/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package kandinsky.fileexplorer.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.border.Border;
import javax.swing.filechooser.FileSystemView;

import ordnereigenschaften.DateiEigenschaften;
import kandinsky.fileexplorer.components.favoritsview.FavoritsView;
import kandinsky.fileexplorer.components.favoritsview.IFavoritViewListener;
import kandinsky.fileexplorer.components.functions.Creator;
import kandinsky.fileexplorer.components.functions.Eigenschaften;
import kandinsky.fileexplorer.components.functions.FileChanger;
import kandinsky.fileexplorer.components.functions.FileClipboard;
import kandinsky.fileexplorer.components.listview.DetailViewCellRenderer;
import kandinsky.fileexplorer.components.listview.GridViewCellRenderer;
import kandinsky.fileexplorer.components.listview.ListView;
import kandinsky.fileexplorer.components.listview.ListViewCellRenderer;
import kandinsky.fileexplorer.components.menubar.MenuBarFileExplorer;
import kandinsky.fileexplorer.components.pathselector.PathSelector;
import kandinsky.fileexplorer.components.pathselector.ISelectedDirectoryListener;
import kandinsky.fileexplorer.components.pfadLeiste.PfadLeiste;
import kandinsky.fileexplorer.components.toolbar.Toolbar;
import kandinsky.fileexplorer.components.toolbar.viewcombobox.ViewComboBox;
import kandinsky.fileexplorer.components.tree.FileSystemTree;
import kandinsky.fileexplorer.config.Favorit;
import kandinsky.fileexplorer.config.Favoriten;
import kandinsky.fileexplorer.config.Konfiguration;
import kandinsky.fileexplorer.errorhandling.ErrorMessage;

/**
 * 
 * @author bawono
 * @edited Philip Hunsicker, Jan Steuer
 * changelog: aender des generierten code, auslageerung Jmenu, dynamische Anordung der Panels aufgrund der conifig datei
 */
@SuppressWarnings("serial")
public class MainFrame extends JFrame {

	
	private static final byte LIST_VIEW = 1;
	private static final byte GRID_VIEW = 2;
	private static final byte DETAIL_VIEW = 3;
	
	// GUI-Elemente (Primitive)
	private ListView ordnerAnsichtFensterRechts;
	private ListView ordnerAnsichtFensterLinks;   
	private FavoritsView favoritsView;
	private FileSystemTree fileSystemTree1;
	private final static boolean RECHTS=true;
	private final static boolean LINKS =false;
	// GUI-Elemente (Container)
	private JTabbedPane paneTreeAndFavs;
	private JPanel rechtesFenster;
	private JPanel linkesFenster ;
	private MenuBarFileExplorer menubar;
	private JSplitPane trennBalkenLinks, trennBalkenRechts;  
	private Toolbar obereToolBar;
	private PfadLeiste pfadLeisteRechts;
	private PfadLeiste pfadLeisteLinks;
	
	// Anderes
	private Konfiguration config ;
	private boolean isRechtesFensterAktiv = true; //muss nicht in die config
	private boolean _onSelectedDirectoryChange_updating = false;
	
	public MainFrame() {
		config = new Konfiguration();
		
		initComponents();
		connectComponents();   //verbinden der Componenten
		observeComponents();  //einbau der Listener
		
		this.pack();
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		
		FileSystemView fsv = FileSystemView.getFileSystemView();
		onSelectedDirectoryChange(fsv.getHomeDirectory());
		
		try {
			GridViewCellRenderer.loadImages();
		} catch (Exception e) {
		}
		
	}
	
	private void initComponents() {
		// Favoriten+Baum Tab
		paneTreeAndFavs = new JTabbedPane(JTabbedPane.TOP);
		favoritsView = new FavoritsView();
		 
		// ListView+PathSelector
		ordnerAnsichtFensterRechts = new ListView();
		ordnerAnsichtFensterLinks = new ListView();
			//ordnerAnsichtFensterLinks = new ListView();
		rechtesFenster = new JPanel();
		linkesFenster = new JPanel();
	
		// Toolbar
		obereToolBar = new Toolbar();
		


		// Rest
		pfadLeisteRechts = new PfadLeiste(new File(config.getInizialPfadRechts()));
		pfadLeisteLinks = new PfadLeiste(new File(config.getInizialPfadLinks()));
		menubar = new MenuBarFileExplorer( config.isZeigeVersteckeDateien());
		
		// Baum
		fileSystemTree1 = new FileSystemTree();
		
		// Trennbalken
		trennBalkenRechts = new JSplitPane();
		trennBalkenLinks =  new JSplitPane();
		
		
		
		
	} 
	private void connectComponents(){
		// Favoriten+Baum Tab
		paneTreeAndFavs.addTab("Dateibaum", null, fileSystemTree1, null);
		paneTreeAndFavs.addTab("Favoriten", null, favoritsView, null);
		
		// ListView+PathSelector
		rechtesFenster.setLayout(new BorderLayout(0, 0));
		rechtesFenster.add(ordnerAnsichtFensterRechts, BorderLayout.CENTER);
		rechtesFenster.add(pfadLeisteRechts, BorderLayout.NORTH);
		linkesFenster.setLayout(new BorderLayout(0, 0));
		linkesFenster.add(ordnerAnsichtFensterLinks, BorderLayout.CENTER);
		linkesFenster.add(pfadLeisteLinks, BorderLayout.NORTH);
		
		
		// PfadLeiste
		pfadLeisteRechts.setVisible(config.isZeigePfadLeisteRechts());  
		pfadLeisteLinks.setVisible(config.isZeigePfadLeisteLinks());
		// JFrame
		setTitle( config.getProgrammName() );
		setMinimumSize( config.getMinFenstergroesse());
		setPreferredSize( config.getOptFenstergroesse());
		
		trennBalkenRechts.setDividerSize( config.getTrennerBreite() );
		trennBalkenRechts.setDividerSize( config.getTrennerBreite() );
		obereToolBar.setVisible(config.isZeigeToolbar());
		
		setJMenuBar( menubar);
		JPanel toolBars = new JPanel();
		toolBars.setLayout(new BorderLayout());
		toolBars.add(obereToolBar, BorderLayout.NORTH);
		//toolBars.add(pfadLeisteRechts, BorderLayout.SOUTH);
		BorderLayout mainfFrameLayout = new BorderLayout();
		getContentPane().setLayout(mainfFrameLayout);
		
		//getContentPane().add(baueMittleresFenster(), BorderLayout.CENTER);

		getContentPane().add(verbindeMittleresFenster(), BorderLayout.CENTER);
		
		getContentPane().add(toolBars, BorderLayout.NORTH);
		
		
		// Baum
		fileSystemTree1.setMinimumSize( config.getMinBaumFenster() );
		
		
		
		
	}

	private void aktuallisiereMittlereFenster(){
		getContentPane().add(verbindeMittleresFenster(), BorderLayout.CENTER);
		this.repaint();
	}
	/**
	 * Baut das mittelerFenster(Baum,Ordnerfenster ..) aufgund der Sichtbarkeites Variablen der Confi klasse auf.
	 * Vorausgesetzt das das rechte OrdnerFenster IMMER angezeigt ist
	 * @return mittleresFenster als Component
	 */
	private Component verbindeMittleresFenster() {

		Component mittleresFenster; //das Teil and das alles verankert ist
		Component baumFenster = paneTreeAndFavs ;
		Component linkesFenster =  this.linkesFenster;
		Component rechtesFenster =  this.rechtesFenster ;
		boolean zeigeBaum = config.isZeigeBaumFenster();
		boolean zeigeLinkesFenster = config.isZeigeLinkesOrdnerFenster();

		
		if(!config.isZeigeRechtesOrdnerFenster()){System.err.print("diese Einstellung wird nicht unterst�tzt: ZeigeRechtesOrdnerFenster=false");}


		if (!config.isZeigeRechtesOrdnerFenster()) {
			System.err
					.print("diese Einstellung wird nicht unterst�tzt: ZeigeRechtesOrdnerFenster=false");
		}
		trennBalkenRechts.setRightComponent(rechtesFenster);
		mittleresFenster = trennBalkenRechts;	
		if( zeigeBaum || zeigeLinkesFenster ){// Wenn mehr als 1 Fenster dargestellt wird
			

			if( zeigeLinkesFenster ){ //Wenn 2 Ordneransichtfenster dargestellt werden
				trennBalkenRechts.setLeftComponent(linkesFenster);
				
				trennBalkenRechts.setDividerLocation( config.getBreiteBaum() + (config.getBreiteFensterLinks()/2));
			}
			if( zeigeBaum ){ // Wenn 1 Ordner und 1 Baum dargestellt werden
				trennBalkenRechts.setLeftComponent(baumFenster);
				trennBalkenRechts.setDividerLocation( config.getBreiteBaum());
			}
			if( zeigeBaum && zeigeLinkesFenster){ // Wenn all Fenster dargestellt werden
				trennBalkenRechts.setLeftComponent(trennBalkenLinks);
				trennBalkenLinks.setLeftComponent(linkesFenster);
				trennBalkenLinks.setRightComponent(baumFenster);
				trennBalkenLinks.setDividerLocation( config.getBreiteFensterLinks());
				trennBalkenRechts.setDividerLocation( config.getBreiteBaum() + config.getBreiteFensterLinks());
			}
			
		}else{//ansonsten stell nur das rechteOdnerFenster dar
			trennBalkenRechts.setLeftComponent(null);
		}
		return mittleresFenster;
	}

	private void observeComponents(){
		
		setFavoritenListener();
		setTreeListener();
		setListViewListener();
		//setPfadLeisteRechtsListener();//beinhalte imoment der Pfadselector bis klar ist wo er dazugeh�rt
		//setPfadLeisteLinksListener();
		setPfadLeisteListener(true); //rechtePfadLeiste = true
		setPfadLeisteListener(false); //Linke Pfadleiste

		//setPfadLeisteListener();// beinhalte imoment der Pfadselector bis klar
								// ist wo er dazugeht

		setToolBarListener();
		setMenuBarListener();
	}
	private void setMenuBarListener() {
		
		menubar.getVerstecktesAnzeigenCheckBox().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				boolean zeigeVersteckte = menubar.getVerstecktesAnzeigen();
				config.setZeigeVersteckeDateien( zeigeVersteckte ); //speichere den Wert in der config datei
				versteckeOrdner(zeigeVersteckte);
				obereToolBar.getHiddenBtn().setSelected(zeigeVersteckte);//setzte toggle den Bottuen in der Toolbar
			}
		});

		menubar.getCopy().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				FileClipboard.copy(getAktuelleView().getSelectedFilesList());
				getAktuelleView().refresh();
			}
		});
		
		menubar.getDelete().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				for (File f : getAktuelleView().getSelectedFilesList()) {
					f.delete();
				}
				getAktuelleView().refresh();
			}
		});
		
		menubar.getPaste().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				FileClipboard.paste(getAktuelleView().getDirectory());
				getAktuelleView().refresh();
			}
		});
		
		menubar.getCut().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				FileClipboard.cut(getAktuelleView().getSelectedFilesList());
				getAktuelleView().refresh();
			}
		});
		
		menubar.getDateiNeuDatei().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String fileName;
				do {
					fileName = FileChanger.getNewFileName();
					if (fileName.isEmpty() || fileName == null)
						new ErrorMessage(getAktuelleView(),
								"Dateiname darf nicht leer sein.",
								ErrorMessage.ERROR);
				} while (fileName.isEmpty() || fileName == null);
				Creator.createFile(fileName, getAktuelleView().getDirectory(), getAktuelleView());
				getAktuelleView().refresh();
			}
		});
		
		menubar.getDateiNeuVerzeichnis().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String dirName;
				do {
					dirName = FileChanger.getNewDirName();
					if (dirName.isEmpty() || dirName == null)
						new ErrorMessage(getAktuelleView(),
								"Verzeichnisname darf nicht leer sein.",
								ErrorMessage.ERROR);
				} while (dirName.isEmpty() || dirName == null);
				Creator.createDirectory(dirName, getAktuelleView().getDirectory(), getAktuelleView());
				getAktuelleView().refresh();
			}
		});
		
		final JFrame hauptFrame = this;
		menubar.getClose().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				hauptFrame.dispose();
			}
		});
		
		
	}
	private void setToolBarListener() {
		// ToolBar
		ViewComboBox viewComboBox = (ViewComboBox) obereToolBar.getSelectViewComboBox();
				viewComboBox.addItemListener(new ItemListener() {
					@Override
					public void itemStateChanged(ItemEvent e) {
						ListView aktuelleView = getAktuelleView();
						if (e.getStateChange() == ItemEvent.SELECTED) {
							String[] strings = (String[]) e.getItem();
							
							if (strings[0].equals("List View")) {
								/*aktuelleView.setLayoutOrientation(javax.swing.JList.VERTICAL_WRAP); 
								aktuelleView.setListViewCellRenderer(new ListViewCellRenderer());
								config.setSelectedView( LIST_VIEW );*/
								aktuelleView.changeView(LIST_VIEW);
								
							} else if (strings[0].equals("Details View")) {
								/*aktuelleView.setLayoutOrientation(javax.swing.JList.HORIZONTAL_WRAP); 
								aktuelleView.setListViewCellRenderer(new DetailViewCellRenderer());
								config.setSelectedView( DETAIL_VIEW );*/
								aktuelleView.changeView(DETAIL_VIEW);
							} else {
								/*aktuelleView.setLayoutOrientation(javax.swing.JList.HORIZONTAL_WRAP); 
								aktuelleView.setListViewCellRenderer(new GridViewCellRenderer());
								config.setSelectedView( GRID_VIEW );*/
								aktuelleView.changeView(GRID_VIEW);
							}
							//aktuallisiereMittlereFenster();//behebt den bug das nach dem wechsle das fenster falsch zusammengebaut wird
						}
					}
				});	

		obereToolBar.getFavoritenBtn().addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						 File selected = getAktuelleView().getDirectory();
						 Favoriten.addFavorit(selected, selected.getName());
						 paneTreeAndFavs.setSelectedComponent(favoritsView);//wechselt beim setzt eines Favorit in die favoriten ansicht
					}
				});		
		obereToolBar.getHiddenBtn().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				boolean zeigeVersteckte = obereToolBar.isVerstecktesAnzeigen();
				config.setZeigeVersteckeDateien( zeigeVersteckte ); //speichere den Wert in der config datei
				versteckeOrdner(zeigeVersteckte);
				menubar.getVerstecktesAnzeigenCheckBox().setSelected(zeigeVersteckte);//setzte toggle den Bottuen in der Toolbar
//				System.out.println(jSplitPane2.getLeftComponent());
			}
		});
		obereToolBar.getZweiFensterBtn().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				boolean isZweiFenster = obereToolBar.isZweiFensterBtn();
				config.setZeigeLinkesOrdnerFenster(isZweiFenster);; //speichere den Wert in der config datei
			
				if(isZweiFenster){
					isRechtesFensterAktiv = LINKS;
					onSelectedDirectoryChange(new File(config.getInizialPfadLinks()));// provesorium das der Fenser
				}else{
					isRechtesFensterAktiv = RECHTS;
				}
				
				aktuallisiereMittlereFenster();
				
			}
		});
		obereToolBar.getVersteckeBaumBtn().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				boolean isZeigeBaume = !obereToolBar.isVersteckeBaumBtn();
				config.setZeigeBaumFenster(isZeigeBaume); 
//				fileSystemTree1.setVisible(isZeigeBaume);
				aktuallisiereMittlereFenster();
			}
		});
		obereToolBar.getNewFileBtn().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ListView aktuellesFenster = getAktuelleView();
				//System.out.println("NeuerOrdner an der stelle:"+aktuellesFenster.getDirectory());
				Creator.createDirectory(config.getNeuerOrdnerName(), aktuellesFenster.getDirectory(), aktuellesFenster);
				aktuellesFenster.refresh();
//				aktuellesFenster.setSelectedDirectory(aktuellesFenster.getDirectory()); //aktuallisiert die View
			}
		});
		obereToolBar.getNewDataBtn().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ListView aktuellesFenster = getAktuelleView();
				//System.out.println("NeuerOrdner an der stelle:"+aktuellesFenster.getDirectory());
				Creator.createFile(config.getNeueDateiName(), aktuellesFenster.getDirectory(), aktuellesFenster);
				aktuellesFenster.refresh();
//				aktuellesFenster.setSelectedDirectory(aktuellesFenster.getDirectory()); //aktuallisiert die View
			}
		});
		obereToolBar.getOrdnerOptionBtn().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//kopiert aus der Popupklasse (actionPerformed() -> else if (e.getSource() == eigenschaftenDateiMItem))
				List<File> file = (List<File>) ordnerAnsichtFensterRechts.getSelectedFilesList();
//				System.out.println("optionbtn:"+file.toString());
				//TODO er nimmt nicht den Selectierte Pfade sondern das Directory m�glicherweise fehler in getSelectedFile();
//				DateiEigenschaften prefDialog = new DateiEigenschaften( file );
//				prefDialog.setVisible(true);
				Eigenschaften.eigenschaften(file);
			}
		});

	}
	
	private ListView getAktuelleView(){
		ListView aktuellesFenster;
		if(isRechtesFensterAktiv ){
			aktuellesFenster = this.ordnerAnsichtFensterRechts;
		}else{
			aktuellesFenster = this.ordnerAnsichtFensterLinks;
		}
		return aktuellesFenster;
	}
	
	private void setPfadLeisteListener(final boolean isRechts) {
		// PfadLeiste
				final PfadLeiste leiste ;
				final ListView view  ;
				//ermoeglich das setztn der listen fuer das rechte und linke fenster in einer MEthode
				if(isRechts){
					view = this.ordnerAnsichtFensterRechts;
					leiste = this.pfadLeisteRechts;
				}else{
					view = ordnerAnsichtFensterLinks;
					leiste = pfadLeisteLinks;
				}	
							
				
				leiste.getpfadEingabe().addListener(new ISelectedDirectoryListener() {
					@Override
					public void pathChange(File selectedPath) {
//						System.out.println("PS CHANGE");
					isRechtesFensterAktiv= isRechts;
						onSelectedDirectoryChange(selectedPath);
						
					}
				});
				leiste.getSuchEingabeJTF().addKeyListener(new KeyAdapter() {
				
						@Override
						public void keyTyped(KeyEvent e) {
							if( config.isAktiveSuche() ){
								sucheStarten(leiste.getSuchEingabeJTF().getText(), view);
							}
						}
						@Override
						public void keyReleased(KeyEvent e) {
							if( config.isAktiveSuche() ){
								sucheStarten(leiste.getSuchEingabeJTF().getText(), view);
							}
						}
					
					
				});
				leiste.getSuchEingabeJTF().addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						if( !config.isAktiveSuche() ){
							sucheStarten(leiste.getSuchEingabeJTF().getText(), view);
						}
					}
				});
				leiste.getSuchSymbol().addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						if( !config.isAktiveSuche() ){
							sucheStarten(leiste.getSuchEingabeJTF().getText(), view);
						}
					}
				});

				leiste.getZurueckBtn().addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						view.geheZurueck();
					}
				});
				leiste.getVorBtn().addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						view.geheVorwarts();
					}
				});
				
			}
	/*private void setPfadLeisteRechtsListener() {
		// PfadLeiste
		
		pfadLeisteRechts.getpfadEingabe().addListener(new ISelectedDirectoryListener() {
			@Override
			public void pathChange(File selectedPath) {
				System.out.println("PS CHANGE");
				onSelectedDirectoryChange(selectedPath);
			}
		});
		pfadLeisteRechts.getSuchEingabeJTF().addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				sucheStarten(pfadLeisteRechts.getSuchEingabeJTF().getText(), ordnerAnsichtFensterRechts);
			}
			@Override
			public void keyReleased(KeyEvent e) {
				sucheStarten(pfadLeisteRechts.getSuchEingabeJTF().getText(), ordnerAnsichtFensterRechts);
			}
		});

		pfadLeisteRechts.getZurueckBtn().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ordnerAnsichtFensterRechts.geheZurueck();
			}
		});
		pfadLeisteRechts.getVorBtn().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ordnerAnsichtFensterRechts.geheVorwarts();
			}
		});
		
	}*/
	/*private void setPfadLeisteLinksListener() {
		// PfadLeiste
		pfadLeisteLinks.getpfadEingabe().addListener(new ISelectedDirectoryListener() {
			@Override
			public void pathChange(File selectedPath) {
				System.out.println("PS CHANGE");
				onSelectedDirectoryChange(selectedPath);
			}
		});
		pfadLeisteLinks.getSuchEingabeJTF().addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				sucheStarten(pfadLeisteRechts.getSuchEingabeJTF().getText(), ordnerAnsichtFensterLinks );
			}
			@Override
			public void keyReleased(KeyEvent e) {
				sucheStarten(pfadLeisteRechts.getSuchEingabeJTF().getText(), ordnerAnsichtFensterLinks);
			}
		});

		pfadLeisteLinks.getZurueckBtn().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ordnerAnsichtFensterLinks.geheZurueck();
			}
		});
		pfadLeisteLinks.getVorBtn().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ordnerAnsichtFensterLinks.geheVorwarts();
			}
		});
		
	}*/
	private void setListViewListener() {
		ordnerAnsichtFensterRechts.addListener(new ISelectedDirectoryListener() {
			@Override
			public void pathChange(File selectedPath) {
//				System.out.println("LV CHANGE");
				isRechtesFensterAktiv = RECHTS;
				onSelectedDirectoryChange(selectedPath);
				
			}
		});
		ordnerAnsichtFensterLinks.addListener(new ISelectedDirectoryListener() {
			@Override
			public void pathChange(File selectedPath) {
//				System.out.println("LV CHANGE");
				isRechtesFensterAktiv = LINKS;//false
				onSelectedDirectoryChange(selectedPath);
				
			}
		});
		
	}
	private void setTreeListener() {
		fileSystemTree1.addListener(new ISelectedDirectoryListener() {
			@Override
			public void pathChange(File selectedPath) {
				onSelectedDirectoryChange(selectedPath);
			}
		});
		
	}
	private void setFavoritenListener() {
		favoritsView.addListener(new IFavoritViewListener() {
			@Override
			public void FavoritSelected(Favorit f) {
				onSelectedDirectoryChange(f.getFile());
			}
		});
	}

	public static void main(String args[]) {
		
		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				new MainFrame().setVisible(true);
				
			}
		});
	}
	
	
	/**
	 * Sucht nach Dateien 
	 * @param eingabe der begriff nachdem gesucht wird
	 * @param listView die Listview in der gesucht wird
	 */
	private void sucheStarten(String eingabe, ListView listView){
		if (eingabe==null || eingabe.isEmpty()){
//			listView.setSelectedDirectory(listView.getDirectory());
			listView.refresh();
		}else{
			listView.sucheOrdner(eingabe);

		}
	}
	
	// Auf Aenderung des selectedDirectory reagieren
	
	private void onSelectedDirectoryChange(File path){
//		System.out.println("aktives Fenster  "+this.isRechtesFensterAktiv);
		
		if(_onSelectedDirectoryChange_updating) return; // verhindert endlosschleifen, da die unten stehenden aufrufe diese mehthode indirekt aufrufen...
		_onSelectedDirectoryChange_updating = true; //interessantes break jan^^ gute idee mit der methode
		
		//pfadLeisteRechts.getpfadEingabe().setSelectedDirectory(path);
		//pfadLeisteLinks.getpfadEingabe().setSelectedDirectory(path);
		//ordnerAnsichtFensterRechts.setSelectedDirectory(path);
		//pfadLeisteRechts.setPfad(path);
		getAktuelleView().setSelectedDirectory(path);

		getAktuellesPfadLeiste().getpfadEingabe().setSelectedDirectory(path);
		getAktuellesPfadLeiste().setPfad(path);
		
		fileSystemTree1.setSelectedDir(path);//TODO baum veraendert sich nicht?
		
		_onSelectedDirectoryChange_updating = false;
	}
	
	private JPanel getAktuellesFenster(){
		JPanel aktuellesFenster;
		if(isRechtesFensterAktiv ){
			aktuellesFenster = rechtesFenster;
		}else{
			aktuellesFenster = linkesFenster;
		}
		return aktuellesFenster;
	}
	private PfadLeiste getAktuellesPfadLeiste(){
		PfadLeiste aktuellesFenster;
		if(isRechtesFensterAktiv ){
			aktuellesFenster = pfadLeisteRechts;
		}else{
			aktuellesFenster = pfadLeisteLinks;
		}
		return aktuellesFenster;
	}
	
	private void versteckeOrdner(boolean zeigeVersteckte) {
		ordnerAnsichtFensterRechts.showHiddenFiles( zeigeVersteckte);
		if(config.isZeigeLinkesOrdnerFenster()){//baeugt einer Nullpoint exception vor
			ordnerAnsichtFensterLinks.showHiddenFiles( zeigeVersteckte);	
		}
	}
	
}