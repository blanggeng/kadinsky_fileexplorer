package kandinsky.fileexplorer.components.menubar;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
/**
 * 
 * @author Philip Hunsicker
 * Ziel: Auslagerung der MenuLeister aus des MainFrames
 * Erstellung 1.09.2014
 * Letzte �nderung 1.09.2014
 * Problem : WiederEinbindung des Listener im Mainframe
 */
@SuppressWarnings("serial")
public class MenuBarFileExplorer extends JMenuBar{

	///nicht der Ort an dem diese Vairable verwaltet werden soll
	
	private JMenuItem file;
	private JMenuItem edit;
	private JMenuItem view;
	private JMenuItem help;
	private JCheckBoxMenuItem hiddenFilesCheckBox;
	private JMenuItem about;
	
	private JMenuItem dateiNeuDatei,  dateiNeuVerzeichnis;
	private JMenuItem close;
	JMenuItem cut;
	JMenuItem copy ;
	JMenuItem paste;
	JMenuItem delete;
	
	
	public MenuBarFileExplorer(boolean hiddenFiles){
		
		file = new JMenu("Datei");
		baueDateiMenupunktNeu();
		baueDateiMenupunktSchliessen();
		add(file);
		
		edit = new JMenu("Bearbeiten");
		baueAusschneidenMenupunkt();
		baueKopierenMenupunkt();
		baueEinfuegenMenupunkt();
		baueLoeschenMenupunkt();
		add(edit);

		view = new JMenu("Ansicht");
		baueHiddenFileMenupunkt(hiddenFiles);
		add(view);
		
		help = new JMenu("?");
		baueAboutMenuItem();
		add(help);
	}
	
	private void baueAboutMenuItem() {
		about = new JMenuItem("�ber");
		about.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				
			}
		});
		help.add(about);
	}

	private void baueHiddenFileMenupunkt(boolean hiddenFiles) {
		hiddenFilesCheckBox = new JCheckBoxMenuItem();
		hiddenFilesCheckBox.setSelected( hiddenFiles );
		hiddenFilesCheckBox.setText("Versteckte Dateien anzeigen");
		hiddenFilesCheckBox.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
						
					}
				});
		
		view.add(hiddenFilesCheckBox);
		
	}

	private void baueLoeschenMenupunkt() {
		delete = new JMenuItem("L�schen");
		delete.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				
			}
		});
		edit.add(delete);
		
	}

	private void baueEinfuegenMenupunkt() {
		paste = new JMenuItem("Einf�gen");
		paste.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				
			}
		});
		edit.add(paste);
		
	}

	private void baueKopierenMenupunkt() {
		copy = new JMenuItem("Kopieren");
		copy.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				
			}
		});
		edit.add(copy);
		
	}

	private void baueAusschneidenMenupunkt() {
		cut = new JMenuItem("Ausschneiden");
		cut.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				
			}
		});
		edit.add(cut);
		
	}

	private void baueDateiMenupunktSchliessen(){
		close = new JMenuItem("Beenden");
		close.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				
			}
		});
		file.add(close);
	}
	
	private void baueDateiMenupunktNeu(){
		
		JMenu dateiNeu = new JMenu("Neu");
		
		dateiNeuDatei = new JMenuItem("Datei");
		dateiNeuDatei.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				
			}
		});
		dateiNeu.add(dateiNeuDatei);
		
		dateiNeuVerzeichnis = new JMenuItem("Verzeichnis");
		dateiNeuVerzeichnis.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				
			}
		});
		dateiNeu.add(dateiNeuVerzeichnis);
		
		file.add(dateiNeu);
		
	}

	public boolean getVerstecktesAnzeigen() {

	  return hiddenFilesCheckBox.isSelected();

	}
	
	/** provisorische Methode das im MainFrame der ActionListener eingebunden werden kanne
	 * TODO besser Loesung
	 * @return checkBox : versteckteDateienAnzeigen
	 */
	public JCheckBoxMenuItem getVerstecktesAnzeigenCheckBox(){
		return hiddenFilesCheckBox;
	}
	
	public JMenuItem getAbout() {
		return about;
	}

	public JMenuItem getHelp() {
		return help;
	}

	public JMenuItem getDateiNeuDatei() {
		return dateiNeuDatei;
	}

	public JMenuItem getDateiNeuVerzeichnis() {
		return dateiNeuVerzeichnis;
	}

	public JMenuItem getCut() {
		return cut;
	}

	public JMenuItem getCopy() {
		return copy;
	}

	public JMenuItem getPaste() {
		return paste;
	}

	public JMenuItem getDelete() {
		return delete;
	}

	public JMenuItem getClose() {
		return this.close;
	}




}
