/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package kandinsky.fileexplorer.components.listview;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.io.File;
import java.text.DecimalFormat;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTree;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;

public class DetailViewCellRenderer extends DefaultListCellRenderer {
	
	// TODO Klasse so umaendern, dass sie eine Element in DetailAnsicht darstellt
	
    private final FileSystemView fileSystemView;
        
    public DetailViewCellRenderer() {
        fileSystemView = FileSystemView.getFileSystemView();
    }

    @Override
    public Component getListCellRendererComponent(
            JList<?> list,
            Object value,
            int index,
            boolean isSelected,
            boolean cellHasFocus) {

        // get the selected node and then convert it to file.
        File file = (File)value;
        
        DetailsViewCell cell = new DetailsViewCell();

        // label here refers to nodes in the tree to give proper
        // icon and name to the nodes.
        
        Icon icon = fileSystemView.getSystemIcon(file);
        String fileName = fileSystemView.getSystemDisplayName(file);
        Long dateModified = file.lastModified();
        String fileType = fileSystemView.getSystemTypeDescription(file);
        String fileSize = getFileSize(file);
        
        if (isSelected){
			 cell.setBackground(Color.lightGray);
		} else {
			 cell.setBackground(Color.white);
		}
        
        cell.setEntry(icon, fileName, fileType, fileSize);
        
        return cell;
    }
    
    private String getFileSize(File file) {
        double byteSize = file.length() / 1024.0;
        DecimalFormat dec = new DecimalFormat("0.00");
        
        return dec.format(byteSize).concat(" KB");
    }
}
