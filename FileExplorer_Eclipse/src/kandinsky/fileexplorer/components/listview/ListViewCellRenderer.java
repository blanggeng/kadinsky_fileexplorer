/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package kandinsky.fileexplorer.components.listview;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTree;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 * Setzt die Ansicht "List View" fuer ein Element um
 * 
 * @author bawono
 */
public class ListViewCellRenderer extends DefaultListCellRenderer {
    private final FileSystemView fileSystemView;
    private final JLabel label;
        
    public ListViewCellRenderer() {
        label = new JLabel();
        label.setOpaque(true);
        fileSystemView = FileSystemView.getFileSystemView();
    }

    @Override
    public Component getListCellRendererComponent(
            JList<?> list,
            Object value,
            int index,
            boolean isSelected,
            boolean cellHasFocus) {

        // get the selected node and then convert it to file.
        File file = (File)value;

        // label here refers to nodes in the tree to give proper
        // icon and name to the nodes.
        label.setIcon(fileSystemView.getSystemIcon(file));
        label.setText(fileSystemView.getSystemDisplayName(file));
        label.setBorder(BorderFactory.createEmptyBorder(2, 8, 2, 0));
        label.setOpaque(isSelected);
        label.setToolTipText(file.getName());
        if(file.isHidden()){
        	label.setOpaque(true);
        	
        	//label.setForeground(new Color(0xdddddd));
        	label.setBackground(new Color(0xaaaaaa));
        }
        
        return label;
    }
}
