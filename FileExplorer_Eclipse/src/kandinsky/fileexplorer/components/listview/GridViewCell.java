package kandinsky.fileexplorer.components.listview;

import javax.swing.JPanel;

import kandinsky.fileexplorer.other.JImagePanel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;

import javax.swing.JLabel;
import java.awt.FlowLayout;
import java.awt.BorderLayout;
import javax.swing.SwingConstants;

/**
 * Ein "Grid View" Element
 * 
 * @author Jan
 *
 */
public class GridViewCell extends JPanel {

	JImagePanel imagePanel;
	JLabel lblNewLabel;
	JPanel panel ;
	
	public GridViewCell() {
		imagePanel = new JImagePanel();
		lblNewLabel = new JLabel("dffffffff");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		panel = new JPanel();
		
		setLayout(null);
		
		imagePanel.setBounds(10, 11, 96, 62);
		imagePanel.setBackground(Color.GRAY);
		add(imagePanel);
		
		panel.setBounds(0, 76, 116, 24);
		add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		
		panel.add(lblNewLabel);

	}
	
	public void setImage(Image img){
		imagePanel.setImageFile(img);
	}
	public void setFileName(String name){
		lblNewLabel.setText(name);
	}
	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(110,100);
	}
	
	@Override
	public void setBackground(Color bg) {
		super.setBackground(bg);
		if(lblNewLabel!=null) lblNewLabel.setBackground(bg);
		if(imagePanel!=null) imagePanel.setBackground(bg);
		if(panel!=null) panel.setBackground(bg);
	}
	
}
