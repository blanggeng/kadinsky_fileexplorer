package kandinsky.fileexplorer.components.listview;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JOptionPane;

import ordnereigenschaften.DateiEigenschaften;
import kandinsky.fileexplorer.components.functions.Delete;
import kandinsky.fileexplorer.components.functions.Eigenschaften;
import kandinsky.fileexplorer.components.functions.FileChanger;
import kandinsky.fileexplorer.components.functions.FileClipboard;
import kandinsky.fileexplorer.components.functions.FileHandler;
import kandinsky.fileexplorer.config.Favoriten;

/**
 * @author Sbattis
 * wird von der ListView verwendet zur Abfrage der Tastatureingaben um SHortcuts zu verwenden
 */
public class KeyHandler implements KeyListener {
	
	private ListView view;
	private javax.swing.JList<File> list;
	
	public KeyHandler(ListView view, javax.swing.JList<File> jList){
		this.view = view;
		this.list = jList;
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if(e.isControlDown()){	
			keyHandleCtrl(e);
		}
		else {
			keyHandleRest(e);
		}
		
	}
	
	/**
	 * behandelt Tastendr�cke bei gedr�ckter Ctrl-Taste
	 * @param e - das Key Event das behandelt werden soll
	 */
	private void keyHandleCtrl(KeyEvent e){
    	switch(e.getKeyCode()){
		case KeyEvent.VK_C:
			copyFiles();
			break;
		case KeyEvent.VK_V:
			pasteFiles();
			break;
		case KeyEvent.VK_X:
			cutFiles();
			break;
		case KeyEvent.VK_F:
			searchFiles();
			break;
		case KeyEvent.VK_E:
			showPreferences();
		}
    }
	
	
    /**
     * Behandelt Tastendr�cke, bei denen keine andere Taste gedr�ckt gehalten wird
     * @param e - das Key Event das behandelt werden soll
     */
    private void keyHandleRest(KeyEvent e){
    	
    	switch(e.getKeyCode()){
    	case KeyEvent.VK_BACK_SPACE:
    		view.geheZurueck();
    		break;
    	case KeyEvent.VK_DELETE:
    		deleteFiles();
    		break;
    	case KeyEvent.VK_F5:
    		refresh();
    		break;
    	case KeyEvent.VK_F2:
    		rename();
    		break;
    	case KeyEvent.VK_ENTER:
    		openFile();
    		break;
    	case KeyEvent.VK_F3:
    		addToFavorits();
    		break;
    		
    	}
    }
    
    /**
     * Aktualisiert in der Listview die Anzeige um ggf neue Dateien anzeigen zu k�nnen
     */
    private void refresh(){
    	view.refresh();
	}
    
    /**
     * Zeigt ein Eigenschaftenfenster an f�r die Dateien, die in der Listview ausgew�hlt wurden.
     */
    private void showPreferences(){
    	List<File> files = new LinkedList<File>();
		for(int i : list.getSelectedIndices()){
			files.add(list.getModel().getElementAt(i));
		}
		Eigenschaften.eigenschaften(files);
    }
    
    /**
     * F�gt in der Listview ausgew�hlte Verzeichnisse den Favoriten hinzu.
     */
    private void addToFavorits(){
		for(int i : list.getSelectedIndices()){
			File fi = list.getModel().getElementAt(i);
			if(fi.isDirectory()){
				Favoriten.addFavorit(fi, fi.getName());
			}
		}
    }
    
    /**
     * Kopiert die in der Listview ausgew�hlten Dateien in die Zwischenablage
     */
    private void copyFiles(){
    	List<File> files = new LinkedList<File>();
		
		for(int i : list.getSelectedIndices()){
			System.out.println(list.getModel().getElementAt(i));
			files.add((File)list.getModel().getElementAt(i));
		}
		FileClipboard.copy(files);
    }
    
    /**
     * Schneidet die in der Listview ausgew�hlten Dateien aus und legt sie in die Zwischenablage
     */
    private void cutFiles(){
    	List<File> files = new LinkedList<File>();
		for(int i : list.getSelectedIndices()){
			files.add(list.getModel().getElementAt(i));
		}
		FileClipboard.cut(files);
    }
    
    /**
     * F�gt dateien aus der Zwischenablage in das aktuelle Verzeichnis der Listview ein.
     */
    private void pasteFiles(){
    	FileClipboard.paste(view.getDirectory());
		
		//GUI aktualisieren
		refresh();
    }
	
	
	/**
	 * L�scht die in der Listview ausgew�hlten Dateien
	 */
	private void deleteFiles(){
		Delete.deleteFiles(list, view);
		
	}
	
	/**
	 * Wenn nur eine Datei ausgew�hlt ist, wird diese umbenannt. 
	 * Daf�r geht ein kleiner Textdialog auf in der der neue Dateiname eingetragen werden kann
	 */
	private void rename(){
		int[] selectedIndices = list.getSelectedIndices();
		if(selectedIndices.length == 1){
			File oldFile = list.getModel().getElementAt(selectedIndices[0]);
			if(oldFile.isDirectory()){
				FileChanger.renameDir(oldFile);
			} else {
				FileChanger.rename(oldFile);
			}
			//GUI aktualisieren
			refresh();
		}
	}
	
	/**
	 * �ffnet die Dateien, die in der Listview ausgew�hlt wurden. Es k�nnen mehrere Dateien ausgew�hlt werden.
	 * Aber wenn mehrere Ordner darunter sind, wird nur der letzte ge�ffnet
	 */
	private void openFile(){
		int[] selectedIndices = list.getSelectedIndices();
		File file;
		
		File resultDir = null;
		
		for(int i = 0; i < selectedIndices.length; i++){
			file = list.getModel().getElementAt(selectedIndices[i]);
			if(file.isDirectory()){
				resultDir = file;
			} else {
				FileHandler.openFile(file, null);
			}
		}
		
		if(resultDir != null){
			view.setSelectedDirectory(resultDir);
		}
	}
	
	/**
	 * Sucht im aktuellen Verzeichnis nach Dateien. Daf�r werden regul�re Ausdr�cke verwendet
	 * Den Suchausdruck gibt man in einem kleinen Dialog an
	 */
	private void searchFiles(){
		String findName = JOptionPane.showInputDialog("regul\u00e4r Ausdruck der zu suchende Dateien: ");
		view.sucheOrdner(findName);
	}

	
}
