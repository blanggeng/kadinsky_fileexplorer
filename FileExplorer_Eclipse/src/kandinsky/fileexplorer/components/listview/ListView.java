 
package kandinsky.fileexplorer.components.listview;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import kandinsky.fileexplorer.other.ViewInterface;

import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;

import kandinsky.fileexplorer.components.contextmenu.Popup;
import kandinsky.fileexplorer.components.functions.FileClipboard;
import kandinsky.fileexplorer.components.functions.FileHandler;
import kandinsky.fileexplorer.components.pathselector.ISelectedDirectoryListener;
import kandinsky.fileexplorer.config.Konfiguration;
import kandinsky.fileexplorer.gui.MainFrame;

/**
 * Das Listview zeigt eine Datei-Liste an. Kontextmenue, Dateisuche, usw werden hier bereitgestellt
 * 
 * @author Alle
 */
public class ListView extends javax.swing.JPanel   {

	private File directory;
	private boolean showHiddenFiles = false;
	private DefaultListModel listModel;
	private KeyHandler keyHandler;
	private List<File> visibleFiles;
	
	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JList<File> jList1;
	private javax.swing.JScrollPane jScrollPane1;
	// End of variables declaration//GEN-END:variables

	// MultiFileSelection wenn man mit geklickter maus ueber die Files zieht
	private boolean _MouseDown = false;
	private Point _startLoc = null;
	private Point _zielLoc = null;
	
	public final byte LIST_VIEW = 1;
	public final byte GRID_VIEW = 2;
	public final byte DETAIL_VIEW = 3;
	
	
	public ListView() {
		final ListView me = this;
		initComponents();
		listModel = new DefaultListModel();
		jList1.setModel(listModel);
		jList1.setCellRenderer(new ListViewCellRenderer());
		jScrollPane1.getVerticalScrollBar().setUnitIncrement(16);
		jScrollPane1.getVerticalScrollBar().setPreferredSize(
				new Dimension(8, 0));
		jScrollPane1.getHorizontalScrollBar().setPreferredSize(
				new Dimension(0, 8));

		jList1.addMouseMotionListener(new MouseMotionAdapter() {

			@Override
			public void mouseDragged(MouseEvent e) {
				if (_MouseDown) {
					_zielLoc = e.getPoint();
					invalidate();
					repaint();
					findElementsFromAreaSelection();
				}
			}

		});

		jList1.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseReleased(MouseEvent e) {
				_MouseDown = false;
				_startLoc = null;
				_zielLoc = null;
				repaint();
			}

			@Override
			public void mouseClicked(MouseEvent evt) {
				
				// Linker button
				if (SwingUtilities.isLeftMouseButton(evt)) {
					JList list = (JList) evt.getSource();
					File ziel;
					
					int index = list.locationToIndex(evt.getPoint());
					if (evt.getClickCount() == 1) {
						if(index==-1){
							jList1.clearSelection();
						}
					}
					
					if (evt.getClickCount() == 2) {
						
						if(index!=-1){
							ziel = jList1.getModel().getElementAt(index);
							if (ziel.isDirectory()) {
								String selectedPath = ziel.toString();
								setSelectedDirectory(new File(selectedPath));
							} else {
								FileHandler.openFile(ziel, null);
							}
						}
					}
					
					
				}
				
				// Rechter button
				if (SwingUtilities.isRightMouseButton(evt)) {
					int index = jList1.locationToIndex(evt.getPoint());
					if (index != -1) {
						String selectedPath = jList1.getModel() .getElementAt(index).toString();
						File selectedFile = (File) jList1.getModel().getElementAt(index);
 						int[] selectedIndices = jList1.getSelectedIndices();
						if (selectedIndices.length > 0) {
							List<File> files = new LinkedList<File>();
							for (int i = 0; i < selectedIndices.length; i++){
								files.add((File) jList1.getModel().getElementAt(selectedIndices[i]));
							}
							
							//files.add((File)jList1.getModel().getElementAt(selectedIndices[index])); // Klappt iwie nicht... komisch
							
							//ist bereits drinnen in der Liste
							//files.add(selectedFile);
							
							new Popup(me, evt, getDirectory(), files);
						} else {
							LinkedList<File> tmp = new LinkedList<File>();
							tmp.add(selectedFile);
							new Popup(me, evt, getDirectory(), tmp);
						}
						System.out.println(selectedPath);
						
					}else{
						new Popup(me, evt, getDirectory());
					}

				}
				 
				
				
				
				
			}

			@Override
			public void mousePressed(MouseEvent evt) {
				_MouseDown = true;
				_startLoc = evt.getPoint();
			}
		});

		keyHandler = new KeyHandler(this, jList1);

		jList1.addKeyListener(keyHandler);
	}
	

	private void findElementsFromAreaSelection() {
		if (_MouseDown && _startLoc != null && _zielLoc != null) {
			int x0 = Math.min(_startLoc.x, _zielLoc.x);
			int x1 = Math.max(_startLoc.x, _zielLoc.x);
			int y0 = Math.min(_startLoc.y, _zielLoc.y);
			int y1 = Math.max(_startLoc.y, _zielLoc.y);

			Rectangle rectangle = new Rectangle(x0, y0, x1 - x0, y1 - y0);
			// TODO iwie die Elemente markieren die unter dem rectangle 'liegen'
		}
	}
	
	/**
	 * Veraendert die Darstellung ("Ansicht") der Dateien 
	 * 
	 * @param style LIST_VIEW, GRID_VIEW, DETAIL_VIEW
	 */
	public void changeView(int style){
		Konfiguration config = new Konfiguration();
		switch(style){
		case LIST_VIEW:			setLayoutOrientation(javax.swing.JList.VERTICAL_WRAP); 
								setListViewCellRenderer(new ListViewCellRenderer());
								config.setSelectedView( LIST_VIEW );
								break;
		case DETAIL_VIEW:		setLayoutOrientation(javax.swing.JList.HORIZONTAL_WRAP); 
								setListViewCellRenderer(new DetailViewCellRenderer());
								config.setSelectedView( DETAIL_VIEW );
								break;
		case GRID_VIEW:			setLayoutOrientation(javax.swing.JList.HORIZONTAL_WRAP); 
								setListViewCellRenderer(new GridViewCellRenderer());
								config.setSelectedView( GRID_VIEW );
								break;
		}
	}
	
	/**
	 * Geht ins uebergeordnete Directory
	 */
	public void geheZurueck(){
		//TODO gehe in den davorigen Pfad zurueck
		//imoment provesorische loesung
		File newDir = getDirectory().getParentFile();
		if(newDir != null){
			setSelectedDirectory(newDir);
		}
	}
	/**
	 * Geht ein Schritt nach oben (wenn zuvor zurueck gegangen wurde)
	 */
	public void geheVorwarts(){
		//Todo gehe vor (nur moeglich wenn davor zurueck gegangen ist
	}
	/**
	 * Soll eine Aktion rueckgaengig machen (umbenennung, verschieben, etc)
	 */
	public void rueckgaenging(){
		//TODO setzt die Letzt kopie, umbennen  verschieb Funktion zurueck
	}
	
	
	private void setListViewCellRenderer(DefaultListCellRenderer renderer) {
		listModel = new DefaultListModel();
		jList1.setModel(listModel);
		jList1.setCellRenderer(new ListViewCellRenderer());

		jList1.setCellRenderer(renderer);
		setSelectedDirectory(directory);
		refreshElementsPanel();

	}

	private void setLayoutOrientation(int orientation) {
		jList1.setLayoutOrientation(orientation);
		refreshElementsPanel();
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		
		// Zeichnet den Bereich der Mausselektion
		if (_MouseDown && _startLoc != null && _zielLoc != null) {

			int x0 = Math.min(_startLoc.x, _zielLoc.x);
			int x1 = Math.max(_startLoc.x, _zielLoc.x);

			int y0 = Math.min(_startLoc.y, _zielLoc.y);
			int y1 = Math.max(_startLoc.y, _zielLoc.y);

			g.setColor(Color.blue);
			g.drawRect(x0, y0, x1 - x0, y1 - y0);
			Color c = new Color(0, 0, 255, 33);
			g.setColor(c);
			g.fillRect(x0, y0, x1 - x0, y1 - y0);
		}
	}


	/**
	 * Sollen versteckte Dateien angezeigt werden?
	 * 
	 * @param value
	 */
	public void showHiddenFiles(boolean value) {
		showHiddenFiles = value;
		setSelectedDirectory(directory);
	}

	/**
	 * @return das aktuelle Directory
	 */
	public File getDirectory() {
		return directory;
	}
	
	public ListModel<File> getSelectedFiles(){
		return jList1.getModel();
	}
	/**
	 *  Wandelt die jList1 in eine List<File> um
	 * @return files in  List<File> format
	 */
	public List<File> getSelectedFilesList(){
		int[] selectedIndices = jList1.getSelectedIndices();
		List<File> files = new LinkedList<File>();
			for (int i = 0; i < selectedIndices.length; i++){
				files.add((File) jList1.getModel().getElementAt(selectedIndices[i]));
			}
		return files;
	}
	
	private void setVisibleFiles(List<File> files) {
		this.visibleFiles = files;
		listModel.clear();
		for (File f : files) {
			if (f.isDirectory() && (showHiddenFiles || !f.isHidden())) {
				listModel.addElement(f);
			}
		}
		for (File f : files) {
			if (f.isFile() && (showHiddenFiles || !f.isHidden())) {
				listModel.addElement(f);
			}
		}

	}
	
	/**
	 *  Refresht die Anzeige, z.B. um Umbennenungen oder Einfuege-Aktionen sichtbar zu machen
	 */
	public void refresh(){
		 //this.setSelectedDirectory(directory);
		 this.setSelectedDirectory(this.getDirectory());
	}

	/**
	 * Setzt das Directory dessen Inhalt angezeigt werden soll
	 * 
	 * @param selectedDirectory
	 */
	public void setSelectedDirectory(File selectedDirectory) {
		if (!selectedDirectory.isDirectory()) {
			return;
			// throw new
			// RuntimeException("FileBrowser kann nur Directories anzeigen");
		}
		// jLabel1.setText(selectedDirectory.getAbsolutePath());
		this.directory = selectedDirectory;
		System.out.println("test:" + selectedDirectory);
		setVisibleFiles(Arrays.asList(selectedDirectory.listFiles()));
		callListeners(selectedDirectory);

	}

	/**
	 * Durchsucht ein Directory nach einem Suchmuter und zeigt die Dateien an. Suchtiefe ist begrenzt!
	 * 
	 * @param selectedDirectory
	 * @param searchPattern
	 */
	private void setSelectedDirectory(File selectedDirectory,
			final String searchPattern) {
		if (!selectedDirectory.isDirectory())
			return;
		this.directory = selectedDirectory;
		FilenameFilter filter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				if (name.toLowerCase().startsWith(searchPattern.toLowerCase()))
					return true;
				return false;
			}
		};

		int ebenen = 2;
		int currentEbene = 0;

		LinkedList<File> openFolders = new LinkedList<>();
		LinkedList<File> foundedFiles = new LinkedList<>();
		openFolders.add(selectedDirectory);

		while (openFolders.size() != 0) {
			if (ebenen <= currentEbene)
				break;
			LinkedList<File> tmpFolders = new LinkedList<>(openFolders);
			while (tmpFolders.size() != 0) {
				File current = tmpFolders.getFirst();
				tmpFolders.remove(current);
				try {
					foundedFiles
							.addAll(Arrays.asList(current.listFiles(filter)));
				} catch (Exception e) {
				}

				for (File x : selectedDirectory.listFiles()) {
					if (x.isDirectory())
						openFolders.add(x);
				}
			}
			currentEbene++;
		}

		setVisibleFiles(foundedFiles);

	}
	/**
	 * @return das aktuelle Directory
	 */
	public File getSelectedDirectory(){
		return this.directory;
	}
	private void refreshElementsPanel() {
		revalidate();
		repaint();
	}

	private void initComponents() {

		jScrollPane1 = new javax.swing.JScrollPane();

		// jList1 = new javax.swing.JList<File>();
		jList1 = new JList<File>() {
			@Override
			public int locationToIndex(Point location) {
				int index = super.locationToIndex(location);
				if (index != -1
						&& !getCellBounds(index, index).contains(location)) {
					return -1;
				} else {
					return index;
				}
			}
		};

		jList1.setLayoutOrientation(javax.swing.JList.VERTICAL_WRAP);
		// jList1.setLayoutOrientation(javax.swing.JList.HORIZONTAL_WRAP);

		jList1.setVisibleRowCount(-1);
		jScrollPane1.setViewportView(jList1);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 400,
				Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 300,
				Short.MAX_VALUE));
	}// </editor-fold>//GEN-END:initComponents

	 
	// Observer Pattern
	private LinkedList<ISelectedDirectoryListener> listeners = new LinkedList<>();
	/**
	 * Fuegt einen SelectedDirectoryListener hinzu. Dieser wird angestossen wenn sich das aktuelle Directory aendert
	 * @param listener
	 */
	public void addListener(ISelectedDirectoryListener listener) {
		listeners.add(listener);
	}
	/**
	 * Entfernt einen Listener
	 * @param listener
	 */
	public void removeListener(ISelectedDirectoryListener listener) {
		listeners.remove(listener);
	}

	private void callListeners(File selected) {
		for (ISelectedDirectoryListener x : listeners) {
			x.pathChange(selected);
		}
	}

/**
 * ruft die interne Methot setSelectedDirectory() auf die eigentlich Sucht aber irgendwie nicht ganz den Korrekten Namen hat. aber irgendie Commit immun ist
 * @param eingabe
 */
	public void sucheOrdner(String eingabe) {
		setSelectedDirectory(this.getDirectory(), 	eingabe);
	}

}
