/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package kandinsky.fileexplorer.components.listview;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Label;
import java.awt.Toolkit;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;

import kandinsky.fileexplorer.config.Favoriten;
import kandinsky.fileexplorer.other.JImagePanel;


/**
 * Setzt die Ansicht "Grid View" fuer ein Element um
 * 
 * @author Jan
 *
 */
public class GridViewCellRenderer extends DefaultListCellRenderer {
	private final FileSystemView fileSystemView;
	// bilddateien
	private static Image img_folder, img_folder_fav, img_file;

			
	public static void loadImages(){
		if(img_folder==null) img_folder=Toolkit.getDefaultToolkit().getImage("images/gridView/folder.png");
		if(img_folder_fav==null) img_folder_fav=Toolkit.getDefaultToolkit().getImage("images/gridView/folderfav.png");
		if(img_file==null) img_file=Toolkit.getDefaultToolkit().getImage("images/gridView/file.png");
	}
	
	public GridViewCellRenderer() {
		fileSystemView = FileSystemView.getFileSystemView();
	}

	@Override
	public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
		loadImages();
		
		GridViewCell cell = new GridViewCell();
		
		File f = (File) value;
		
		if(isSelected){
			 cell.setBackground(Color.lightGray);
		}else{
			 cell.setBackground(Color.white);
		}
		
		if(f.isDirectory()){
			if(Favoriten.containsFile(f)){
				cell.setImage(img_folder_fav);
			}else{
				cell.setImage(img_folder);
			}
		}else{
			cell.setImage(img_file);
		}
		
		cell.setFileName(f.getName());
		return cell;
	}

	 

}
