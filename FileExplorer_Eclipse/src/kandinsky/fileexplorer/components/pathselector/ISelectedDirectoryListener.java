package kandinsky.fileexplorer.components.pathselector;

import java.io.File;

/**
 * Informiert den Observer, dass das aktuelle Directory sich geaendert hat
 * 
 * @author Jan
 *
 */
public interface ISelectedDirectoryListener {
	public void pathChange(File selectedPath);
}
