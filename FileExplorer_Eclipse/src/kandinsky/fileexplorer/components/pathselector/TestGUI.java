package kandinsky.fileexplorer.components.pathselector;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Color;
import java.io.File;

public class TestGUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TestGUI frame = new TestGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TestGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 833, 403);
		contentPane = new JPanel();
		contentPane.setBackground(Color.LIGHT_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		PathSelector pathSelector = new PathSelector();
		pathSelector.setBounds(10, 11, 797, 35);
		contentPane.add(pathSelector);
		
		
		
		// So reagiert man im uebergeordneten GUI-Element auf aenderungen
		pathSelector.addListener(new ISelectedDirectoryListener() {
			@Override
			public void pathChange(File selectedPath) {
				setTitle(selectedPath.getAbsolutePath());
			}
		});
		
		// so setzt man den akutellen pfad
		pathSelector.setSelectedDirectory(new File("C:/Windows/"));
	}
}
