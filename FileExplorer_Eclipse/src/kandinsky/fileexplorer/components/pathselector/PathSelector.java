package kandinsky.fileexplorer.components.pathselector;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;

import javax.swing.JLabel;
import javax.swing.filechooser.FileSystemView;

import kandinsky.fileexplorer.components.pathselector.ISelectedDirectoryListener;

import java.awt.FlowLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;


/**
 * Zeigt den Pfad zu einem Directoy an. Zusaetzlich kann auf einen Unterordner des Pfades geklickt werden. 
 * Ueber ein ISelectedDirectory-ObserverPattern koennen andere Komponenten darauf reagieren.
 * 
 * @author Jan
 *
 */
public class PathSelector extends javax.swing.JPanel {

	private File selectedDirectory;
	private HashMap<Object, File> mappingHashMap = new HashMap<>();
	private FileSystemView fileSystemView = FileSystemView.getFileSystemView();
	
	public PathSelector() {
		setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
	}

	public File getSelectedDirectory() {
		return selectedDirectory;
	}

	private Component getElement(File file){
		JLabel label = new JLabel();
		label.setOpaque(true);
		//label.setText("/ " + file.getName());
		label.setText(  fileSystemView.getSystemDisplayName(file) + " ");
		//label.setBorder(BorderFactory.createEmptyBorder(2, 8, 2, 0));
		//label.setOpaque(isSelected);
		//label.setOpaque(cellHasFocus);
		//label.setBackground(Color.gray);
		
		label.setMaximumSize(label.getPreferredSize());
		
		return label;
	}
	/**
	 * Setzt das aktuelle Directory
	 * @param selectedDirectory
	 */
	public void setSelectedDirectory(File selectedDirectory) {
		if (!selectedDirectory.isDirectory())
			return;
		this.selectedDirectory = selectedDirectory;
		LinkedList<File> elementsFiles = new LinkedList<>();
		File currentFile = selectedDirectory;
		while (currentFile != null) {
			elementsFiles.add(currentFile);
			currentFile = currentFile.getParentFile();
		}
		
		this.mappingHashMap.clear();
		this.removeAll();
		
		
		for (int i = elementsFiles.size() - 1; i >= 0; i--) {
			File file = elementsFiles.get(i);
			Component tmpComponent = getElement(file);
			mappingHashMap.put(tmpComponent, file);
			this.add(tmpComponent);
			tmpComponent.addMouseListener(new MouseListener(){
				@Override
				public void mouseClicked(MouseEvent e) {
					setSelectedDirectory(mappingHashMap.get(e.getSource()));
				}
				@Override
				public void mouseEntered(MouseEvent e) {
					Component tmpComponent = (Component) e.getSource();
					tmpComponent.setBackground(Color.gray);
					tmpComponent.repaint();
				}
				@Override
				public void mouseExited(MouseEvent e) {
					Component tmpComponent = (Component) e.getSource();
					tmpComponent.setBackground(getBackground());
					tmpComponent.repaint();
				}
				@Override
				public void mousePressed(MouseEvent e) {
				}
				@Override
				public void mouseReleased(MouseEvent e) {
				}
			});
			
			
			
			
		}
		callListeners(selectedDirectory);
		
		revalidate();
		repaint();
		
	}
 
	// Listener
	private LinkedList<ISelectedDirectoryListener> listeners = new LinkedList<>();
	/**
	 * Fuegt einen Listener hinzu
	 * 
	 * @param listener
	 */
	public void addListener(ISelectedDirectoryListener listener) {
		listeners.add(listener);
	}
	/**
	 * Entfernt einen Listener
	 * 
	 * @param listener
	 */
	public void removeListener(ISelectedDirectoryListener listener) {
		listeners.remove(listener);
	}
	private void callListeners(File selected) {
		for (ISelectedDirectoryListener x : listeners) {
			x.pathChange(selected);
		}
	}

}
