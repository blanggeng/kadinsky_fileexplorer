/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package kandinsky.fileexplorer.components.toolbar.viewcombobox;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

/**
 *
 * @author bawono
 */
public class ViewComboBox extends JComboBox<Object> {
    
    private final DefaultComboBoxModel model;
    
    public ViewComboBox() {
        model = new DefaultComboBoxModel();
        setModel(model);
        setRenderer(new ViewComboBoxRenderer());
        //setEditor(new ViewComboBoxEditor());
        
        addItems(viewList);
    }
    
    private void addItems(String[][] items) {
        for (String[] anItem : items) {
            model.addElement(anItem);
        }
    }
    
    private final String[][] viewList = {{"List View", "listViewIcon.png"}, 
                                   {"Details View", "detailsViewIcon.png"},
                                   {"Grid View", "gridViewIcon.png"}};
}