/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package kandinsky.fileexplorer.components.toolbar.viewcombobox;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

/**
 *
 * @author bawono
 */
public class ViewComboBoxRenderer extends JPanel implements ListCellRenderer {

    private JLabel labelItem = new JLabel();
     
    public ViewComboBoxRenderer() {
        setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.weightx = 1.0;
        constraints.insets = new Insets(2, 2, 2, 2);
         
        labelItem.setOpaque(true);
        labelItem.setHorizontalAlignment(JLabel.LEFT);
         
        add(labelItem, constraints);
        setBackground(Color.LIGHT_GRAY);
    }
    
    public JLabel getSelectedLabel() {
        return this.labelItem;
    }
     
    @Override
    public Component getListCellRendererComponent(JList list, Object value,
            int index, boolean isSelected, boolean cellHasFocus) {
        String[] item = (String[]) value;
        labelItem.setOpaque(false);
        labelItem.setName(item[0]);
        // set country flag
        labelItem.setIcon(new ImageIcon(getClass().getResource(item[1])));
        labelItem.setText(item[0]);
        if (isSelected) {
            //labelItem.setBackground(Color.BLUE);
            labelItem.setOpaque(true);
            
            
            //labelItem.setForeground(Color.YELLOW);
        } else {
            //labelItem.setForeground(Color.BLACK);
            //labelItem.setBackground(Color.LIGHT_GRAY);
        }
         
        return this;
    }
    
}
