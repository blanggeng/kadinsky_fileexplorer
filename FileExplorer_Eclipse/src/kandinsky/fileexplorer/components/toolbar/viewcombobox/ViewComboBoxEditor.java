/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package kandinsky.fileexplorer.components.toolbar.viewcombobox;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.plaf.basic.BasicComboBoxEditor;

/**
 *
 * @author bawono
 */
public class ViewComboBoxEditor extends BasicComboBoxEditor {
    
    private JPanel panel = new JPanel();
    private ImageIcon imageIcon = new ImageIcon();
    private String selectedValue;
     
    public ViewComboBoxEditor() {
        panel.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.weightx = 1.0;
        constraints.insets = new Insets(2, 5, 2, 2);
         
        //labelItem.setOpaque(false);
        //labelItem.setHorizontalAlignment(JLabel.LEFT);
        //labelItem.setForeground(Color.WHITE);
         
        //panel.add(imageIcon, constraints);
        //panel.setBackground(Color.BLUE);       
    }
     
    public Component getEditorComponent() {
        return this.panel;
    }
     
    @Override
    public Object getItem() {
        return this.selectedValue;
    }
     
    @Override
    public void setItem(Object item) {
        if (item == null) {
            return;
        }
        String[] view = (String[]) item;
        selectedValue = view[0];
    } 
}
