/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package kandinsky.fileexplorer.components.toolbar;

import java.awt.BorderLayout;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;

import kandinsky.fileexplorer.other.LanguageDE;
import kandinsky.fileexplorer.components.toolbar.viewcombobox.*;
/**
 *
 * @author bawono, Philip Hunsicker
 * changelog 03.09.2014 einbindnen von weiteren symbole Philip Hunsicker, �berarbeited des Codes
 */
@SuppressWarnings("serial")
public class Toolbar extends JPanel {

	final static String IMAGEPFAD 	= "/images/"; // auslagerung config
//	final static int DEFAULTBREITE 	= 50;
//	final static int DEFAULTHOEHE 	= 50 ;
	ImageIcon favoritenIco;
	ImageIcon newDataIco 	;
	ImageIcon newFileIco;
	ImageIcon copyToIco;
	ImageIcon ordnerOptionIco;
	ImageIcon hiddenIco;
	ImageIcon zweiFensterIco;
	ImageIcon hilfeIco;
	ImageIcon versteckeBaumIco;
	JButton favoritenBtn = new JButton();
	JButton newDataBtn = new JButton();
	JButton newFileBtn = new JButton();
	JButton copyToBtn = new JButton();
	JButton ordnerOptionBtn = new JButton();
	JToggleButton zweiFensterBtn = new JToggleButton();
	JToggleButton hiddenBtn = new JToggleButton();
	JToggleButton versteckeBaum = new JToggleButton();


	JButton hilfeBtn = new JButton();
    private ViewComboBox SelectViewComboBox = new ViewComboBox();
	

	JToolBar leftToolBar = new JToolBar();
	JToolBar rightToolBar = new JToolBar();
	

    // End of variables declaration//GEN-END:variables
	
	
    /**
     * Creates new form Toolbar
     */
    public Toolbar() {
    	 weiseIconZu();
    	 weiseTooltipsZu();
		 this.setLayout(new BorderLayout());
		 this.add(baueLinkeBar(), BorderLayout.WEST);
		 this.add(baueRechteBar(), BorderLayout.EAST);
         
    }

	private JToolBar baueRechteBar() {
		rightToolBar.setBorder(null);
		rightToolBar.setFloatable(false); //verhindert das verschieben der Toolbar
		rightToolBar.setRollover(true);
		
		SelectViewComboBox.setToolTipText("Select View");
		rightToolBar.add(SelectViewComboBox);
		rightToolBar.add(hilfeBtn);
		hilfeBtn.setVisible(false);//Hilfe nicht implementiert

        SelectViewComboBox.getAccessibleContext().setAccessibleName("viewComboBox");
        return rightToolBar;
	}
	private JToolBar baueLinkeBar() {
		leftToolBar.setFloatable(false); //verhindert das verschieben der Toolbar
		leftToolBar.setRollover(true);
		
		leftToolBar.add(newDataBtn);
		leftToolBar.add(newFileBtn);
		leftToolBar.add(copyToBtn);
		leftToolBar.add(favoritenBtn);
		leftToolBar.add(ordnerOptionBtn);
		leftToolBar.add(hiddenBtn);
		leftToolBar.add(zweiFensterBtn);
		leftToolBar.add(versteckeBaum);
		return leftToolBar;
	}
	private void weiseIconZu(){
		String absuluterPfad = new File("").getAbsolutePath() + IMAGEPFAD;
		newDataIco = erstelleImageIcon(absuluterPfad+"newData.png");  
		newFileIco = erstelleImageIcon(absuluterPfad+"newFile.png");  
		copyToIco = erstelleImageIcon(absuluterPfad+"help.png");  
		ordnerOptionIco = erstelleImageIcon(absuluterPfad+"FolderOption.png");  
		hiddenIco = erstelleImageIcon(absuluterPfad+"hideFolder.png");  
		zweiFensterIco = erstelleImageIcon(absuluterPfad+"newWindow.png");  
		hilfeIco = erstelleImageIcon(absuluterPfad+"help.png");  
		versteckeBaumIco = erstelleImageIcon(absuluterPfad+"showTree.png");
		favoritenIco = Toolbar.erstelleImageIcon(absuluterPfad+"addfavoriten.png"); 
		
		newDataBtn.setIcon(newDataIco);
		newFileBtn.setIcon(newFileIco);
		copyToBtn.setIcon(copyToIco);
		copyToBtn.setVisible(false);// wiedereinbinden des CopyTo buttons
		ordnerOptionBtn.setIcon(ordnerOptionIco);
		zweiFensterBtn.setIcon(zweiFensterIco);
		hiddenBtn.setIcon(hiddenIco);
		versteckeBaum.setIcon(versteckeBaumIco);
		hilfeBtn.setIcon(hilfeIco);
		favoritenBtn.setIcon(favoritenIco);
		
		/*
		newDataBtn.setText("nD");
		newFileBtn.setText("nF");
		copyToBtn.setText("cTo");
		ordnerOptionBtn.setText("Oo");
		zweiFensterBtn.setText("2W");
		hiddenBtn.setText("SH");
		versteckeBaum.setText("hB");
		hilfeBtn.setText("?");
		*/
	}
    
	private void weiseTooltipsZu() {
		 newDataBtn.setToolTipText(LanguageDE.TTNEWDATA);
		 newFileBtn.setToolTipText(LanguageDE.TTNEWFILE);
		 copyToBtn.setToolTipText(LanguageDE.TTCOPYTO); 
		 ordnerOptionBtn.setToolTipText(LanguageDE.TTFILEOPTION); 
		 zweiFensterBtn.setToolTipText(LanguageDE.TTADDLEFTWINDOW); 
		 hiddenBtn.setToolTipText(LanguageDE.TTSHOWHIDEN); 
		 versteckeBaum.setToolTipText(LanguageDE.TTHIDETREE); 
		 favoritenBtn.setToolTipText(LanguageDE.TTADDFAVORITE); 
	}
	
	
	
	
	/* 
	 * Ruft ob an dem Pfad ein Images vorhanen ist, wenn ja wir ein ImageIcon zurueckgegeben
	 * @return ImageIcon , null
	 */
	public static ImageIcon erstelleImageIcon(String path) {
//	    java.net.URL imgURL = getClass().getResource(path);
	    File file = new File(path);
	    
	      if (file.exists()) {
	       return new ImageIcon(file.toString());
	    } else {
	        System.err.println("Symbol konnte nicht geladen werden: " + path);
//	        System.err.println("standarpfad:"+new File("").getAbsolutePath());
//	        System.err.println("kompletterpfad:"+new File(path).getAbsolutePath());
	        return null;
	    }
	 
	}
	//SETTER GETTER--------------------------------------------------------------
	public JToggleButton getHiddenBtn() {
		return hiddenBtn;
	}
	public ViewComboBox getSelectViewComboBox() {
		return SelectViewComboBox;
	}
	public boolean isVerstecktesAnzeigen(){
		return hiddenBtn.isSelected();
	}
	public JToggleButton getZweiFensterBtn() {
		return zweiFensterBtn;
	}
	public boolean isZweiFensterBtn() {
		return zweiFensterBtn.isSelected();
	}
	public JToggleButton getVersteckeBaumBtn() {
		return versteckeBaum;
	}
	public boolean isVersteckeBaumBtn() {
		return versteckeBaum.isSelected();
	}
	public JButton getNewDataBtn() {
		return newDataBtn;
	}
	public JButton getNewFileBtn() {
		return newFileBtn;
	}
	public JButton getCopyToBtn() {
		return copyToBtn;
	}
	public JButton getOrdnerOptionBtn() {
		return ordnerOptionBtn;
	}
	public JButton getFavoritenBtn() {
		return favoritenBtn;
	}
}
