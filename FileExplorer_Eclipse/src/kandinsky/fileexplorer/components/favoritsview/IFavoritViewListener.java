package kandinsky.fileexplorer.components.favoritsview;

import kandinsky.fileexplorer.config.Favorit;

/**
 * Teilt dem Beobachter mit, dass ein Favorit ausgewaehlt wurde
 * 
 * @author Jan
 *
 */
public interface IFavoritViewListener {
	/**
	 * Favorit wurde ausgewaehlt
	 * 
	 * @param f
	 */
	public void FavoritSelected(Favorit f);
}
