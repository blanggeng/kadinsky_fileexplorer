package kandinsky.fileexplorer.components.favoritsview;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.File;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import kandinsky.fileexplorer.config.Favorit;
import kandinsky.fileexplorer.config.Favoriten;

public class TestGUIFavs extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TestGUIFavs frame = new TestGUIFavs();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TestGUIFavs() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 350, 487);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		FavoritsView favoritsView = new FavoritsView();
		contentPane.add(favoritsView, BorderLayout.CENTER);
		
		
		// Ein paar favoriten erstellen
		if(Favoriten.getFavoriten().size()==0){
			System.out.println("Ein paar Favoriten erstellen...");
			Favoriten.addFavorit(new File("C:/"), "Ein Favorit hier");
			Favoriten.addFavorit(new File("C:/"), "Ein Favorit da");
			Favoriten.addFavorit(new File("C:/"), "Test");
			Favoriten.addFavorit(new File("C:/"), "blaaa");
			Favoriten.addFavorit(new File("C:/"), "asdf");
			Favoriten.addFavorit(new File("C:/"), "nocheiner");
		}
		
		
		// Auf selektionen reagieren
		favoritsView.addListener(new IFavoritViewListener() {
			@Override
			public void FavoritSelected(Favorit f) {
				setTitle("Gew�hlter Favorit: " + f.getBezeichnung());
			}
		});
		
	}
	
	
	
	

}
