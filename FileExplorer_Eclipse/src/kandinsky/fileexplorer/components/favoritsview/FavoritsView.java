/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package kandinsky.fileexplorer.components.favoritsview;

import java.awt.Dimension;
import java.awt.PopupMenu;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.LinkedList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import kandinsky.fileexplorer.config.Favorit;
import kandinsky.fileexplorer.config.Favoriten;
import kandinsky.fileexplorer.config.IFavoritenListener;

/**
 * Stellt eine Liste von Favoriten dar und erlaubt das Editieren mit einem einfachen Kontextmenue.
 * Beobachter koennen ueber ein FavoritViewListener mitbekommen wenn ein Favorit ausgewaehlt wurde
 * 
 * @author Jan
 *
 */
public class FavoritsView extends javax.swing.JPanel implements
		IFavoritenListener {

	// GUI-Variablen
	private DefaultListModel<Favorit> listModel;
	private javax.swing.JList<Favorit> jList1;
	private javax.swing.JScrollPane jScrollPane1;

	public FavoritsView() {
		initComponents();
		listModel = new DefaultListModel<Favorit>();
		jList1.setModel(listModel);
		jList1.setCellRenderer(new FavoritCellRenderer());
		jScrollPane1.getVerticalScrollBar().setUnitIncrement(16);
		jScrollPane1.getVerticalScrollBar().setPreferredSize(
				new Dimension(8, 0));
		jScrollPane1.getHorizontalScrollBar().setPreferredSize(
				new Dimension(0, 8));

		jList1.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent evt) {
				if (SwingUtilities.isLeftMouseButton(evt)) {
					JList<Favorit> list = (JList<Favorit>) evt.getSource();
					int index = list.locationToIndex(evt.getPoint());
					Favorit ziel = jList1.getModel().getElementAt(index);
					callAllListeners_FavSelected(ziel);
				}
				refreshElementsPanel();
			}

			@Override
			public void mousePressed(MouseEvent evt) {
				JList<Favorit> list = (JList<Favorit>) evt.getSource();
				if (SwingUtilities.isRightMouseButton(evt)) {
					int index = list.locationToIndex(evt.getPoint());
					final Favorit selected = jList1.getModel().getElementAt(
							index);

					JPopupMenu menu = new JPopupMenu();
					menu.add(new AbstractAction("entfernen") {
						@Override
						public void actionPerformed(ActionEvent arg0) {
							Favoriten.removeFavorit(selected);
						}
					});
					menu.add(new AbstractAction("umbenennen") {
						@Override
						public void actionPerformed(ActionEvent arg0) {

							String response = JOptionPane.showInputDialog(null,
									"Favorit '" + selected.getBezeichnung()
											+ "' umbenennen",
									"Neue Bezeichnung: ",
									JOptionPane.PLAIN_MESSAGE);

							if(response!=null && !response.isEmpty()){
								selected.setBezeichnung(response);
								Favoriten.saveFavs();
								Favoriten.updateAllListeners();
								repaint();
							}
							

						}
					});

					menu.show(evt.getComponent(), evt.getX(), evt.getY());
				}
			}

		});

		jList1.setLayoutOrientation(JList.VERTICAL);
		jList1.setVisibleRowCount(1);
		
		Favoriten.addListener(this);
		Favoriten.loadFavs();
	}

	// GUI-Code
	private void updateView() {
		listModel.clear();
		List<Favorit> favs = Favoriten.getFavoriten();
		for (Favorit f : favs) {
			listModel.addElement(f);
		}
		refreshElementsPanel();
	}

	private void refreshElementsPanel() {
		revalidate();
		repaint();
	}

	private void initComponents() {

		jScrollPane1 = new javax.swing.JScrollPane();
		jList1 = new javax.swing.JList<Favorit>();

		jList1.setLayoutOrientation(javax.swing.JList.VERTICAL_WRAP);
		// jList1.setLayoutOrientation(javax.swing.JList.HORIZONTAL_WRAP);

		jList1.setVisibleRowCount(-1);
		jScrollPane1.setViewportView(jList1);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 400,
				Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 300,
				Short.MAX_VALUE));
	}// </editor-fold>//GEN-END:initComponents

	// ObserverPattern
	private LinkedList<IFavoritViewListener> listeners = new LinkedList<>();
	public void addListener(IFavoritViewListener listener) {
		listeners.add(listener);
	}
	public void removeListener(IFavoritViewListener listener) {
		listeners.remove(listener);
	}
	private void callAllListeners_FavSelected(Favorit f) {
		for (IFavoritViewListener x : listeners) {
			x.FavoritSelected(f);
		}
	}

	// Favoriten-Liste aendert sich
	@Override
	public void favsChange() {
		updateView();
	}

}
