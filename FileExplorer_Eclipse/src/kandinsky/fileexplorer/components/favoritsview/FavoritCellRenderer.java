/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package kandinsky.fileexplorer.components.favoritsview;

import java.awt.Component;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.filechooser.FileSystemView;

import kandinsky.fileexplorer.config.Favorit;

/**
 * Stellt einen Favorit-Element dar
 * 
 * @author Jan
 */
public class FavoritCellRenderer extends DefaultListCellRenderer {
    private final FileSystemView fileSystemView;
    private final JLabel label;
        
    public FavoritCellRenderer() {
        label = new JLabel();
        label.setOpaque(true);
        fileSystemView = FileSystemView.getFileSystemView();
    }

    @Override
    public Component getListCellRendererComponent(
            JList<?> list,
            Object value,
            int index,
            boolean isSelected,
            boolean cellHasFocus) {

        // get the selected node and then convert it to file.
        Favorit current = (Favorit)value;

        // label here refers to nodes in the tree to give proper
        // icon and name to the nodes.
        label.setIcon(fileSystemView.getSystemIcon(current.getFile()));
        label.setText(current.getBezeichnung());
        label.setBorder(BorderFactory.createEmptyBorder(2, 8, 2, 0));
        label.setOpaque(false);        
        return label;
    }
}
