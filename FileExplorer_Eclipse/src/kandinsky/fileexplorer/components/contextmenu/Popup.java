package kandinsky.fileexplorer.components.contextmenu;

import java.awt.Component;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.WindowConstants;

import ordnereigenschaften.DateiEigenschaften;
import kandinsky.fileexplorer.components.functions.Creator;
import kandinsky.fileexplorer.components.functions.Eigenschaften;
import kandinsky.fileexplorer.components.functions.FileChanger;
import kandinsky.fileexplorer.components.functions.FileClipboard;
import kandinsky.fileexplorer.components.functions.FileHandler;
import kandinsky.fileexplorer.components.listview.ListView;
import kandinsky.fileexplorer.components.tree.FileSystemTree;
import kandinsky.fileexplorer.errorhandling.ErrorMessage;

/**
 * 
 * @author Giuseppe Patane Erzeugt ein Kontextmen�
 *
 */

public class Popup extends JPopupMenu implements ActionListener {

	private static final long serialVersionUID = 1L;

	// Rechtsklick auf Datei/ Verzeichnis
	JMenuItem ausschneidenMItem, einfuegenMItem, kopierenMItem,
			dateiOeffnenMItem, verzeichnisOeffnenMItem, loeschenMItem,
			umbenennenMItem, eigenschaftenDateiMItem;

	// Rechtsklick ins Leere
	JMenuItem dateiNeuMItem, verzeichnisNeuMItem, aktualisierenMItem,
			sortierenMItem, eigenschaftenAktuellesVerzeichnisMItem,
			listenAnsichtMItem, kachelnAnsichtMItem, detailAnsichtMItem;

	private List<File> files; // Eine Liste von Dateien/ Verzeichnissen
	private File directory; // Das Verzeichnis in dem sich der User gerade
							// befindet

	// private Component parentComp;
	private ListView parentCompListView;
	private FileSystemTree parentCompTree;
	private Component parent;

	public static final byte LIST_VIEW = 1;
	public static final byte GRID_VIEW = 2;
	public static final byte DETAIL_VIEW = 3;

	public static final byte FILE_SYSTEM_TREE = 2;

	private byte view;

	/*
	 * public Popup(ListView parentComp, MouseEvent event, File directory,
	 * ListView listView) { this(parentComp, event, directory); this.parentComp=
	 * listView; }
	 */

	// Konstruktor f�r den Baum (Klick ins Leere)
	/**
	 * Konstruktor f�r den FileSystemTree (Baumstruktur) bei Klick ins Leere
	 * 
	 * @param parentComp
	 *            Component in welchem sich das Kontextmen� befindet
	 * @param event
	 *            Mausevent welches ausgel�st wird (Rechtsklick)
	 * @param directory
	 *            Verzeichnis in dem sich der FileExporer aktuell befindet
	 */
	public Popup(FileSystemTree parentComp, MouseEvent event, File directory) {
		this.parentCompTree = parentComp;
		this.parent = parentComp;
		initialize(event, directory);
		view = FILE_SYSTEM_TREE;
		createMenuLeer(FILE_SYSTEM_TREE);

		show(event.getComponent(), event.getX(), event.getY());
	}

	// Konstruktor f�r den Baum (Klick auf Datei(en))
	/**
	 * Konstruktor f�r den FileSystemTree (Baumstruktur) bei Klick auf Datei(en)
	 * 
	 * @param parentComp
	 *            Component in welchem sich das Kontextmen� befindet
	 * @param event
	 *            Mausevent welches ausgel�st wird (Rechtsklick)
	 * @param directory
	 *            Verzeichnis in dem sich der FileExporer aktuell befindet
	 * @param files
	 *            Liste von Dateien auf die geklickt wurde
	 */
	public Popup(FileSystemTree parentComp, MouseEvent event, File directory,
			List<File> files) {
		initialize(event, directory, files);
		this.parentCompTree = parentComp;
		this.parent = parentComp;
		view = FILE_SYSTEM_TREE;
		createMenuFile(FILE_SYSTEM_TREE);

		show(event.getComponent(), event.getX(), event.getY());
	}

	// Rechtsklick ins Leere
	/**
	 * Konstruktor f�r die ListView (Fenster) bei Klick ins Leere
	 * 
	 * @param parentComp
	 *            Component in welchem sich das Kontextmen� befindet
	 * @param event
	 *            Mausevent welches ausgel�st wird (Rechtsklick)
	 * @param directory
	 *            Verzeichnis in dem sich der FileExporer aktuell befindet
	 */
	public Popup(ListView parentComp, MouseEvent event, File directory) {
		this.parentCompListView = parentComp;
		this.parent = parentComp;
		initialize(event, directory);
		view = LIST_VIEW;
		createMenuLeer(LIST_VIEW);

		show(event.getComponent(), event.getX(), event.getY());
	}

	// Rechtsklick auf Datei(en)/ Verzeichnis(e)
	/**
	 * Konstruktor f�r die ListView (Fenster) bei Klick auf Datei(en)
	 * 
	 * @param parentComp
	 *            Component in welchem sich das Kontextmen� befindet
	 * @param event
	 *            Mausevent welches ausgel�st wird (Rechtsklick)
	 * @param directory
	 *            Verzeichnis in dem sich der FileExporer aktuell befindet
	 * @param files
	 *            Liste von Dateien auf die geklickt wurde
	 */
	public Popup(ListView parentComp, MouseEvent event, File directory,
			List<File> files) {
		this.parentCompListView = parentComp;
		this.parent = parentComp;
		initialize(event, directory, files);
		view = LIST_VIEW;
		createMenuFile(LIST_VIEW);

		show(event.getComponent(), event.getX(), event.getY());
	}

	private void initialize(MouseEvent event, File directory, List<File> files) {
		this.files = files;
		initialize(event, directory);
	}

	private void initialize(MouseEvent event, File directory) {
		this.directory = directory;
	}

	/**
	 * Erzeugt die Men�eintr�ge beim Klick auf Datei(en)
	 * 
	 * @param view
	 *            Gibt an in welcher View der Rechtsklick ausgef�hrt wurde und
	 *            erzeugt anh�ngig davon die entsprechenden Eintr�ge
	 */
	private void createMenuFile(int view) {
		if (view == LIST_VIEW) {
			if (files != null) {
				if (!files.get(0).isDirectory()) {
					dateiOeffnenMItem = new JMenuItem("\u00D6ffnen");
					dateiOeffnenMItem.addActionListener(this);
					/*
					 * verzeichnisOeffnenMItem = new JMenuItem("Verzeichnis");
					 * verzeichnisOeffnenMItem.addActionListener(this);
					 * menu.add(verzeichnisOeffnenMItem);
					 */
					add(dateiOeffnenMItem);
				}
			}
		}
		ausschneidenMItem = new JMenuItem("Ausschneiden");
		ausschneidenMItem.addActionListener(this);
		add(ausschneidenMItem);

		kopierenMItem = new JMenuItem("Kopieren");
		kopierenMItem.addActionListener(this);
		add(kopierenMItem);

		einfuegenMItem = new JMenuItem("Einf\u00FCgen");
		einfuegenMItem.addActionListener(this);
		add(einfuegenMItem);

		loeschenMItem = new JMenuItem("L\u00F6schen");
		loeschenMItem.addActionListener(this);
		add(loeschenMItem);

		umbenennenMItem = new JMenuItem("Umbenennen");
		umbenennenMItem.addActionListener(this);
		add(umbenennenMItem);

		eigenschaftenDateiMItem = new JMenuItem("Eigenschaften");
		eigenschaftenDateiMItem.addActionListener(this);
		add(eigenschaftenDateiMItem);
	}

	/**
	 * Erzeugt die Men�eintr�ge beim Klick ins Leere
	 * 
	 * @param view
	 *            Gibt an in welcher View der Rechtsklick ausgef�hrt wurde und
	 *            erzeugt anh�ngig davon die entsprechenden Eintr�ge
	 */
	private void createMenuLeer(int view) {
		JMenu menu = new JMenu("Neu");

		dateiNeuMItem = new JMenuItem("Textdatei");
		dateiNeuMItem.addActionListener(this);
		menu.add(dateiNeuMItem);
		/*
		 * verzeichnisNeuMItem = new JMenuItem("Verzeichnis");
		 * verzeichnisNeuMItem.addActionListener(this);
		 * menu.add(verzeichnisNeuMItem);
		 */
		add(menu);

		if (view == LIST_VIEW) {
			JMenu ansicht = new JMenu("Ansicht");

			listenAnsichtMItem = new JMenuItem("Listen Ansicht");
			listenAnsichtMItem.addActionListener(this);
			ansicht.add(listenAnsichtMItem);

			kachelnAnsichtMItem = new JMenuItem("Kachel Ansicht");
			kachelnAnsichtMItem.addActionListener(this);
			ansicht.add(kachelnAnsichtMItem);

			detailAnsichtMItem = new JMenuItem("Detail Ansicht");
			detailAnsichtMItem.addActionListener(this);
			ansicht.add(detailAnsichtMItem);

			add(ansicht);
		}

		einfuegenMItem = new JMenuItem("Einf\u00FCgen");
		einfuegenMItem.addActionListener(this);
		add(einfuegenMItem);

		sortierenMItem = new JMenuItem("Aktualisieren");
		sortierenMItem.addActionListener(this);
		add(sortierenMItem);

		eigenschaftenDateiMItem = new JMenuItem("Eigenschaften");
		eigenschaftenDateiMItem.addActionListener(this);
		add(eigenschaftenDateiMItem);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println(e.getActionCommand());

		if (e.getSource() == dateiOeffnenMItem) {
			for (int i = 0; i < files.size(); i++) {
				File curFile = files.get(i);
				if (!curFile.isDirectory())
					FileHandler.openFile(curFile, parentCompListView);
			}

		} else if (e.getSource() == verzeichnisOeffnenMItem) {

		} else if (e.getSource() == ausschneidenMItem) {
			FileClipboard.cut(files);

		} else if (e.getSource() == kopierenMItem) {
			FileClipboard.copy(files);

		} else if (e.getSource() == einfuegenMItem) {
			FileClipboard.paste(directory);
			parentCompListView.setSelectedDirectory(directory);

		} else if (e.getSource() == dateiNeuMItem) {
			String fileName;
			do {
				fileName = FileChanger.getNewFileName();
				if (fileName.isEmpty() || fileName == null)
					new ErrorMessage(parentCompListView,
							"Dateiname darf nicht leer sein.",
							ErrorMessage.ERROR);
			} while (fileName.isEmpty() || fileName == null);
			Creator.createFile(fileName, directory, parentCompListView);

		} else if (e.getSource() == verzeichnisNeuMItem) {
			String dirName;
			do {
				dirName = FileChanger.getNewDirName();
				if (dirName.isEmpty() || dirName == null)
					new ErrorMessage(parentCompListView,
							"Verzeichnisname darf nicht leer sein.",
							ErrorMessage.ERROR);
			} while (dirName.isEmpty() || dirName == null);
			Creator.createDirectory(dirName, directory, parentCompListView);

		} else if (e.getSource() == aktualisierenMItem) {

		} else if (e.getSource() == eigenschaftenDateiMItem) {
			Eigenschaften.eigenschaften(files);
			/*if (files != null && !files.isEmpty()) {
				if (files.size() > 1) {
					Eigenschaften.eigenschaften(files);
				} else {
					DateiEigenschaften prefDialog = new DateiEigenschaften(
							files.get(0));
					prefDialog.setVisible(true);
				}
			} else {
				DateiEigenschaften prefDialog = new DateiEigenschaften(
						directory);
				prefDialog.setVisible(true);
			}*/
		} else if (e.getSource() == listenAnsichtMItem) {
			parentCompListView.changeView(LIST_VIEW);
		} else if (e.getSource() == kachelnAnsichtMItem) {
			parentCompListView.changeView(GRID_VIEW);
		} else if (e.getSource() == detailAnsichtMItem) {
			parentCompListView.changeView(DETAIL_VIEW);
		} else if (e.getSource() == sortierenMItem) {

		} else if (e.getSource() == loeschenMItem) {
			for (File f : files) {
				f.delete();
			}
		} else if (e.getSource() == umbenennenMItem) {
			int filesCount = files.size();
			for (int i = 0; i < filesCount; i++) {
				FileChanger.rename(files.get(i));
			}
		}

		parentCompListView.setSelectedDirectory(parentCompListView
				.getSelectedDirectory());

	}

}
