package kandinsky.fileexplorer.components.contextmenu;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.event.PopupMenuEvent;

import kandinsky.fileexplorer.components.listview.ListView;

/**
 * 
 * @author Giuseppe Patane
 * Klasse zum Testen des Kontextme�s
 * keine weitere Funktion
 *
 */

public class KontextMenu extends JFrame implements MouseListener {
	
	private static final long serialVersionUID = 1L;
	JMenuItem bmi, ami, emi, kmi, dmi, vmi;
	private List<File> files;
	private int fileCount;
	private File directory;
	private ListView listViewParent = null;
	
	public KontextMenu(ListView sender, File directory){
		super("Kontextmenu");
		this.listViewParent = sender;
		this.directory = directory;
		fileCount = 0;
        addMouseListener(this); 
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public KontextMenu(ListView sender, File directory, List<File> file){
		this(sender, directory);
		this.files = file;
		fileCount = file.size();
	}
	
	public List<File> getFiles(){
		return files;
	}
	
	private void setMenu(MouseEvent event, ListView sender) {
		if(fileCount == 0){
			if (event.isPopupTrigger()) { 
				new Popup(sender, event, directory);
			}
        } else{
        	if(event.isPopupTrigger()){
        		new Popup(sender, event, directory, files);
        	}
        }
    }

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent event) {
		setMenu(event,	listViewParent );

	}
	
	public static void main(String[] args) {
		File dir = new File("./");
		KontextMenu frame = new KontextMenu(null,dir);
        frame.setLocation(200, 100); 
        frame.setSize(300, 200); 
        frame.setVisible(true); 

    } 

}
