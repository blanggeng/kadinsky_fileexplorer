package kandinsky.fileexplorer.components.pfadLeiste;


import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.File;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;

import kandinsky.fileexplorer.components.toolbar.Toolbar;// nur zum benutzen
import kandinsky.fileexplorer.components.pathselector.PathSelector;
import kandinsky.fileexplorer.components.toolbar.viewcombobox.*;
import kandinsky.fileexplorer.other.LanguageDE;
/**
 *
 * @author  Philip Hunsicker
 */
@SuppressWarnings("serial")
public class PfadLeiste extends JPanel{
	/*
	 * To change this license header, choose License Headers in Project Properties.
	 * To change this template file, choose Tools | Templates
	 * and open the template in the editor.
	 */



		final static String IMAGEPFAD 	= "/images/"; //TODO auslagerung config

		JButton suchSymbol = new JButton();

		JButton vorBtn = new JButton();
		JButton zurueckBtn = new JButton();
		JButton zuletztVerwendetBtn = new JButton();


		ImageIcon suchSymbolIco ;  
		ImageIcon vorIco   ;
		ImageIcon zurueckIco;   

		
		JTextField pfadEingabeJTF = new JTextField();

		JTextField suchEingabeJTF = new JTextField(); 
		PathSelector pfadEingabeBoxen = new PathSelector();

//		JButton hilfeBtn = new JButton();
		

		JToolBar leftToolBar = new JToolBar();
		JToolBar rightToolBar = new JToolBar();
		

	    // End of variables declaration//GEN-END:variables
		
		
	    /**
	     * Erzeugt "Brotkrummen" Pfad Leiste
	     * @param initzialPfad	Pfad mit dem die Leiste erzeugt wird
	     */
		public PfadLeiste(File initzialPfad) {
	    	 pfadEingabeBoxen.setSelectedDirectory(initzialPfad);
	    	 weiseIconZu();
	    	 weiseTooltipsZu();
			 this.setLayout(new BorderLayout());
			 this.add(baueLinkeBar(), BorderLayout.WEST);
			 this.add(baueRechteBar(), BorderLayout.EAST);
			 this.add(pfadEingabeBoxen , BorderLayout.CENTER);
			 // this.add(pfadEingabeJTF , BorderLayout.CENTER);
	    }
		
		/**
		 * Erzeugt die rechte Leiste und gibt diese zur�ck
		 * @return		erzeugte Leiste
		 */
		private JToolBar baueRechteBar() {
			rightToolBar.setBorder(null);
			rightToolBar.setFloatable(false); //verhindert das verschieben der Toolbar
			rightToolBar.setRollover(true);

			
			//suchEingabeJTF.setText("suche");
			suchEingabeJTF.setPreferredSize(new Dimension(70,0));//Todo groe�e �ber config datei
			rightToolBar.add(suchSymbol);
			rightToolBar.add(suchEingabeJTF);
			
	        return rightToolBar;
		}
		
		/**
		 * Erzeugt die linke Leiste und gibt diese zur�ck
		 * @return		erzeugte Leiste
		 */
		private JToolBar baueLinkeBar() {
			leftToolBar.setFloatable(false); //verhindert das verschieben der Toolbar
			leftToolBar.setRollover(true);
			
			leftToolBar.add(zurueckBtn);
			leftToolBar.add(vorBtn);// vor funktion nicht implementiert
			vorBtn.setVisible(false);
			leftToolBar.add(zuletztVerwendetBtn);//
			zuletztVerwendetBtn.setVisible(false);//Todo einbinden zuletzt verwendet

			

			return leftToolBar;
		}
		
		/**
		 * Setzt die Vor-/ Zur�ck Icons und die Lupe f�r den Such Button
		 */
		private void weiseIconZu(){
			//TODO Tooltips
			String absuluterPfad = new File("").getAbsolutePath() + IMAGEPFAD;
			suchSymbolIco = Toolbar.erstelleImageIcon(absuluterPfad+"Zoom-icon.png");  
			vorIco = Toolbar.erstelleImageIcon(absuluterPfad+"for-icon.png");  
			zurueckIco = Toolbar.erstelleImageIcon(absuluterPfad+"back-icon.png");  

			
			suchSymbol.setIcon(suchSymbolIco);
			vorBtn.setIcon(vorIco);
			zurueckBtn.setIcon(zurueckIco);
			zuletztVerwendetBtn.setIcon(Toolbar.erstelleImageIcon(absuluterPfad+"KeinSymbol.png"));

//			zuletztVerwendetBtn.setVisible(false);
			/*
			suchSymbol.setText("Q");
			vorBtn.setText(">");
			zurueckBtn.setText("<");
			zuletztVerwendetBtn.setText("\\Z/");

			favoritenBtn.setText("\\F/");
//			hilfeBtn.setText("?");
			*/
			
		}
	    
		/**
		 * Erzeugt Tooltips
		 */
		private void weiseTooltipsZu(){
			
			 suchSymbol.setToolTipText(LanguageDE.TTSEARCH);
			 vorBtn.setToolTipText(LanguageDE.TTFOR); 
			 zurueckBtn.setToolTipText(LanguageDE.TTBACK);
			 zuletztVerwendetBtn.setToolTipText(LanguageDE.TTLASTUSED);

		}
		
		//SETTER GETTER--------------------------------------------------------------


		public JTextField getPfadEingabeJTF() {
			return pfadEingabeJTF;
		}
//		public PathSelector getPfadEingabeBoxen() {
//			return pfadEingabeBoxen;
//		}
		public void setPfad(File neuerPfad) {
//			 pfadEingabeBoxen.setSelectedDirectory(neuerPfad);
			 pfadEingabeJTF.setText( neuerPfad.toString() );
			
		}
		
		public JButton getVorBtn() {
			return vorBtn;
		}
		public JButton getZurueckBtn() {
			return zurueckBtn;
		}
		public JButton getZuletztVerwendetBtn() {
			return zuletztVerwendetBtn;
		}

		public JTextField getSuchEingabeJTF() {
			return suchEingabeJTF;
		}
		public PathSelector getpfadEingabe() {
			return pfadEingabeBoxen;
		}
		public JButton getSuchSymbol() {
			return suchSymbol;
		}
	}
	

