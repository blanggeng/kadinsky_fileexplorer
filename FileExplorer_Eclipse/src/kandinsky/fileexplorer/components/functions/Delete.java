package kandinsky.fileexplorer.components.functions;

import java.io.File;

import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import kandinsky.fileexplorer.components.listview.ListView;

public class Delete {

	
	
	
	
	public static void deleteFiles(JList<File> list , ListView view){
		File delFile;
		//Kopiert aus dem Keyhandler
		int abfrage = JOptionPane.showConfirmDialog(null, "Die Daten k\u00F6nnen nicht wiederhergestellt werden. Sind Sie sicher?", 
				"L\u00F6chen...", JOptionPane.YES_NO_OPTION);
		
		if(abfrage == JOptionPane.YES_OPTION){
			for(int i : list.getSelectedIndices()){
				delFile = list.getModel().getElementAt(i);
				//TODO: momentan dauerhaftes loeschen, da Papierkorb ueber Java scheinbar nicht richtig erreichbar
				delFile.delete();	
			}
			//GUI aktualisieren
			view.refresh();
		}
	}

}
