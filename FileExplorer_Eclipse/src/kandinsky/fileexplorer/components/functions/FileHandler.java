package kandinsky.fileexplorer.components.functions;

import java.awt.Component;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermission;
import java.util.HashSet;

import kandinsky.fileexplorer.errorhandling.ErrorMessage;

public class FileHandler {

	public FileHandler(){}
	
	/**
	 * �ffnet Dateien mit dem im Betriebssystem voreingestellten Programm (z.B. "�ffnen mit..." unter Windows)
	 * @param file		Zu �nnende Datei
	 * @param parent	View in dem sich der User befindet
	 */
	public static void openFile(File file, Component parent){
		Desktop desk = Desktop.getDesktop();
		try {
			desk.open(file);
		} catch (IOException e1) {
			new ErrorMessage(parent, "Datei konnte nicht ge�ffnet werden.", ErrorMessage.ERROR);
			throw new RuntimeException("Ausf�hrungsfehler: " + e1);
		}
	}
	
	/**
	 * Setzt die Dateirechte f�r eine Datei. Unter Windows funktioniert das setzen der Berechtigungen nicht
	 * @param file - die Datei die ge�ndert werden soll
	 * @param read - das Leserecht (true = kann lesen)
	 * @param write - das Schreibrecht (true = kann schreiben)
	 * @param exec - das Ausf�hrrecht (true = kann ausf�hren)
	 */
	public static void setPermissions(File file, boolean read, boolean write, boolean exec){
		//wenn Posix
		if(FileSystems.getDefault().supportedFileAttributeViews().contains("posix")){
			HashSet<PosixFilePermission> perms = new HashSet<PosixFilePermission>();
			if(read){
				perms.add(PosixFilePermission.OWNER_READ);
			}
			if(write){
				perms.add(PosixFilePermission.OWNER_WRITE);
			}
			if(exec){
				perms.add(PosixFilePermission.OWNER_EXECUTE);
			}
			
			
			try {
				Files.setPosixFilePermissions(file.toPath(), perms);
			} catch (IOException e) {
				throw new RuntimeException("Kann Berechtigungen nicht \u00e4ndern");
			}
		}
	}
	
	
	
	
	/**
	 * setzt den Versteckstatus einer Datei. Funktioniert nur unter Windows Systemen
	 * @param file - zu verarbeitende Datei
	 * @param hidden - true wenn versteckt werden soll, false wenn sichtbar sein soll
	 */
	public static void setFileHidden(File file, boolean hidden){
		try {
			Files.setAttribute(file.toPath(), "dos:hidden", hidden);
		} catch (IOException e) {
			throw new RuntimeException("Kann Datei nicht bearbeiten!");
		}
	}
}
