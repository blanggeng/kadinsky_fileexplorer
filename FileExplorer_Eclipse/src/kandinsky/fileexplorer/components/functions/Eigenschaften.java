package kandinsky.fileexplorer.components.functions;

import java.io.File;
import java.util.List;

import ordnereigenschaften.DateiEigenschaften;
import ordnereigenschaften.MultiDateiEigenschaften;

public class Eigenschaften {

	
	
	public static void eigenschaften(List<File> files){
		if(files != null){
			for(File f : files){
				System.out.println(f);
			}
			
			if (files.size() == 1) {
				DateiEigenschaften prefDialog = new DateiEigenschaften(
						files.get(0));
				prefDialog.setVisible(true);
			} else if(files.size() > 1) {
				MultiDateiEigenschaften prefDialog = new MultiDateiEigenschaften(files);
				prefDialog.setVisible(true);
			}
		}
	}	
}
