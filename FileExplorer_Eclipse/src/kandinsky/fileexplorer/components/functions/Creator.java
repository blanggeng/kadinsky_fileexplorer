package kandinsky.fileexplorer.components.functions;

import java.awt.Component;
import java.io.File;
import java.io.IOException;

import kandinsky.fileexplorer.errorhandling.ErrorMessage;
/**
 * 
 * @author Giuseppe Patane
 * Creatorklasse zum erzeugen von Dateien und Verzeichnissen
 *
 */
public class Creator {

	public Creator(){ }
	
	
	/**
	 * Erzeugt eine neue Datei
	 * @param fileName		Dateiname der neuen Datei
	 * @param path			Pfad in dem die Datei erzeugt werden soll
	 * @param parentComp	Die View in dem sich der User gerade befindet
	 */
	public static void createFile(String fileName, File path, Component parentComp){
		File newFile = new File(path.toString() + "/" + fileName);
		try{
			newFile.createNewFile();
		} catch(IOException exception){
			new ErrorMessage(parentComp, "Datei mit diesem Namen ist bereits vorhanden", ErrorMessage.WARNING);
		}
	}

	/**
	 * Erzeugt ein neues Verzeichnis
	 * @param fileName		Verzeichnisname der neuen Verzeichnises
	 * @param path			Pfad in dem das Verzeichnis erzeugt werden soll
	 * @param parentComp	Die View in dem sich der User gerade befindet
	 */
	public static void createDirectory(String dirName, File path, Component parentComp) {
		File newDir = new File(path.toString() + "/" + dirName);
		try{
			newDir.mkdir();
		} catch(Throwable exception){
			new ErrorMessage(parentComp, "Verzeichnis mit diesem Namen ist bereits vorhanden", ErrorMessage.WARNING);
		}
	}
	
}
