package kandinsky.fileexplorer.components.functions;

import java.io.File;

import javax.swing.JOptionPane;

public class FileChanger {

	public FileChanger(){}
	
	/**
	 * Erzeugt eine Messagebox mit Eingabefeld und gibt dessen Eingabe zur�ck
	 * @param text		Text der in der Messagebox ausgegeben werden kann
	 * @return			Die Eingabe im Textfeld
	 */
	private static String getInputDialog(String text){
		return JOptionPane.showInputDialog(text);
	}
	
	/**
	 * Methode zum Umbenennen von Dateien
	 * @param oldFile	Datei welche umbenannt werden soll
	 */
	public static void rename(File oldFile){
		File newFile = new File(oldFile.getParent(), getNewFileName());
		oldFile.renameTo(newFile);
	}
	
	/**
	 * Methode zum Umbenennen von Verzeichnissen
	 * @param oldDir	Verzeichnis welches umbenannt werden soll
	 */
	public static void renameDir(File oldDir){
		File newDir = new File(oldDir.getParent(), getNewDirName());
		oldDir.renameTo(newDir);
	}
	
	public static String getNewFileName(){
		return getInputDialog("Geben Sie den Dateinamen ein.");
	}

	public static String getNewDirName() {
		return getInputDialog("Geben Sie den Verzeichnisnamen ein.");
	}
	
}
