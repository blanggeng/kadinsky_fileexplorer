package kandinsky.fileexplorer.components.functions;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Sbattis
 *
 * Erm�glicht das Kopieren, ausschneiden und Einf�gen von beliebig vielen Dateien gleichzeitig
 */
public class FileClipboard {

	static private boolean cutted = false;
	
	private FileClipboard(){
		
	}
	
	/**
	 * leert die Zwischenablage
	 */
	static public void clearContent(){		
		//leere Liste einf�gen
		ClipboardFiles clipFiles = new ClipboardFiles(new LinkedList<File>());
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(clipFiles, null);
		cutted = false;
	}
	
	/**
	 * Legt Dateien in die Zwischenablage
	 * @param files - Liste an Dateien, die kopiert werden sollen
	 */
	static public void copy(List<File> files){
		ClipboardFiles clipFiles = new ClipboardFiles(files);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(clipFiles, null);
		cutted = false;
	}
	
	/**
	 * Legt Dateien in die Zwischenablage und l�scht diese, wenn sie kopiert wurden
	 * @param files - Liste aller Dateien, die ausgeschnitten werden sollen
	 */
	static public void cut(List<File> files){
		ClipboardFiles clipFiles = new ClipboardFiles(files);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(clipFiles, null);
		cutted = true;
		
	}
	
	
	/**
	 * F�gt alle Dateien in der Zwischenablage in ein bestimmtes Verzeichnis ein. Wenn die Zwischenablage keine Dateien enth�lt tut die Methode nichts
	 * @param directory - Zielverzeichnis in dem die Dateien hineinkopiert werden
	 */
	static public void paste(File directory){
		File dest;
		Clipboard clipb = Toolkit.getDefaultToolkit().getSystemClipboard();

		if(clipb.isDataFlavorAvailable(DataFlavor.javaFileListFlavor)){
			List<File> data;
			try {
				data = (List<File>)clipb.getData(DataFlavor.javaFileListFlavor);
			} catch (UnsupportedFlavorException e) {
				throw new RuntimeException("Ung�ltiger Datentyp im CLipboard!");
			} catch (IOException e) {
				throw new RuntimeException("Clipboardfehler!");
			}
			
			for(File f : data){
				dest = new File(directory.getPath(), f.getName());
				try {
					if(cutted){
						Files.move(f.toPath(), dest.toPath(), java.nio.file.StandardCopyOption.ATOMIC_MOVE);
					} else {
						Files.copy(f.toPath(), dest.toPath(), java.nio.file.StandardCopyOption.COPY_ATTRIBUTES);
					}
				} catch (IOException e1) {
					throw new RuntimeException("Fehler beim einf�gen!");
				}
			}
			if(cutted){
				clearContent();
			}
		}
		
		
		
	}
	
	/**
	 * pr�ft ob die Kopien in der Zwischenablage ausgeschnitten sind oder mehrmals kopiert werden k�nnen
	 * @return
	 */
	static public boolean isCutted(){
		return cutted;
	}

}

//wird nur von der FileClipboardklasse ben�tigt um Dateien zu kopieren
class ClipboardFiles implements Transferable, ClipboardOwner {

	private List<File> list;

	public ClipboardFiles(List<File> list) {
		this.list = list;
	}
	
	public List<File> getData(){
		return list;
	}

	@Override
	public Object getTransferData(DataFlavor flavor) {

		if (flavor == DataFlavor.javaFileListFlavor) {
			return list;
		}

		throw new RuntimeException("");
	}

	@Override
	public DataFlavor[] getTransferDataFlavors() {
		DataFlavor[] dataFlavors = new DataFlavor[1];
		dataFlavors[0] = DataFlavor.javaFileListFlavor;
		return dataFlavors;
	}

	@Override
	public boolean isDataFlavorSupported(DataFlavor flavor) {

		if (flavor == DataFlavor.javaFileListFlavor) {
			return true;
		}

		return false;
	}

	@Override
	public void lostOwnership(java.awt.datatransfer.Clipboard clipboard,
			Transferable contents) {
	}

}


