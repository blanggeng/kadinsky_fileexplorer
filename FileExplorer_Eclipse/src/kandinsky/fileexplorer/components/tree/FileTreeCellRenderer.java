package kandinsky.fileexplorer.components.tree;


import java.awt.Component;
import java.io.File;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

// TreeCellRenderer for rendering each item (file/folder) in tree.
    public class FileTreeCellRenderer extends DefaultTreeCellRenderer {

        private final FileSystemView fileSystemView;
        private final JLabel label;
        
        FileTreeCellRenderer() {
            label = new JLabel();
            label.setOpaque(true);
            fileSystemView = FileSystemView.getFileSystemView();
        }

        @Override
        public Component getTreeCellRendererComponent(
            JTree tree,
            Object value,
            boolean selected,
            boolean expanded,
            boolean leaf,
            int row,
            boolean hasFocus) {
            
            // get the selected node and then convert it to file.
            DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;
            File file = (File)node.getUserObject();
            
            // some extra stuff to give padding between the nodes in tree.
            int padding = 2;
            
            if (node.toString().equals("Desktop") || node.toString().equals("Documents")) {
                padding = 15;
            }
            
            // label here refers to nodes in the tree to give proper
            // icon and name to the nodes.
            label.setIcon(fileSystemView.getSystemIcon(file));
            label.setText(fileSystemView.getSystemDisplayName(file));
            label.setBorder(BorderFactory.createEmptyBorder(padding, 0, padding, 0));
            if (selected) {
                label.setBackground(backgroundSelectionColor);
                label.setForeground(textSelectionColor);
            } else {
                label.setBackground(backgroundNonSelectionColor);
                label.setForeground(textNonSelectionColor);
            }

            return label;
        }
    }