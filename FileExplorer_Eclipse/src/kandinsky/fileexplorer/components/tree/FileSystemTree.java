/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package kandinsky.fileexplorer.components.tree;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.LinkedList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.TreePath;

import kandinsky.fileexplorer.components.contextmenu.Popup;
import kandinsky.fileexplorer.components.pathselector.ISelectedDirectoryListener;

/**
 *
 * @author Bawono Langgeng
 * 
 *         Custom Component JTree zum Zeigen von filesystemview. just drag and
 *         drop this class file to JSplitpane and the it will do all the task
 *         for file system tree.
 */
public class FileSystemTree extends JPanel  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTree tree;
	private DefaultTreeModel treeModel;
	private FileSystemView fileSystemView;
	private File selectedDir;
	//TODO: Bawono oder wer auch immer das machen will:
	//ich hab mir jetzt mal meine eigene variable erstellt, aber 
	//selectedDir obendr�ber sollte ne LinkedList<File> sein, da man ja
	//auch mehrere Ordner im Baum ausw�hlen kann. Wenn ihr das gemacht habt
	//k�nnt ihr selectedDirectorys l�schen und es an entsprechender stelle ab�ndern
	//d�rfte ja nur da sein wo entschieden wird welches Kontextmen� erstellt wird.
	private LinkedList<File> selectedDirectorys;
	
	private final FileSystemTree me = this;

	// simple constructor
	public FileSystemTree() {
		selectedDirectorys = new LinkedList<>();

		setTree();
		setLayout(new BorderLayout());
		JScrollPane scrollPane = new JScrollPane(tree);
		scrollPane.getVerticalScrollBar().setUnitIncrement(16);
		scrollPane.getVerticalScrollBar().setPreferredSize(new Dimension(8, 0));
		scrollPane.getHorizontalScrollBar().setPreferredSize(
				new Dimension(0, 8));
		add(scrollPane, BorderLayout.CENTER);

		tree.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			//TODO: Funktioniert noch nicht, muss mal gucken wo es h�ngt erzeugt kein Kontextmen� oder zeigt es nicht an
			//	...Fortsetzung folgt...
			
			@Override
			public void mouseClicked(MouseEvent evt) {
				if (SwingUtilities.isRightMouseButton(evt)) {
					if(selectedDirectorys.isEmpty()){
						new Popup(me, evt, getSelectedDirectory());
					}else{
						new Popup(me, evt, getSelectedDirectory(), selectedDirectorys);
					}
				}
			}
		});
	}

	public File getSelectedDirectory() {
		return this.selectedDir;
	}

	public void setSelectedDir(File dir) {
		System.out.println("baumpfad" + selectedDir);
		selectedDir = dir;
		// tree.setSelectionPath(new TreePath(dir));
		// TODO: aktuallisieren des Baumes anhand des �bergebenen Pfad

	}

	// set Tree to have a filesystem hierarchy.
	private void setTree() {
		selectedDir = new File("C:/");				//TODO: Und was ist wenn es ein UNIX System ist?
		DefaultMutableTreeNode root = new DefaultMutableTreeNode();

		// show the file system roots.
		fileSystemView = FileSystemView.getFileSystemView();
		for (File f : fileSystemView.getRoots()) {

			FileTreeNode node1 = new FileTreeNode(f);
			root.add(node1);
			File file = fileSystemView.getDefaultDirectory();
			FileTreeNode node2 = new FileTreeNode(file);
			root.add(node2);
		}

		treeModel = new DefaultTreeModel(root);
		tree = new JTree(treeModel);
		tree.setCellRenderer(new FileTreeCellRenderer());
		tree.setRootVisible(false);
		tree.setShowsRootHandles(true);

		// listener for when item is selected.
		tree.getSelectionModel().addTreeSelectionListener(
				(new TreeSelectionListener() {
					@Override
					public void valueChanged(TreeSelectionEvent e) {
						FileTreeNode node = (FileTreeNode) tree
								.getLastSelectedPathComponent();
						Object selectedNode = node.getUserObject();
						File selected = new File(selectedNode.toString());
						selectedDir = selected;
						//TODO: Das dann bitte Ab�ndern
						selectedDirectorys = new LinkedList<File>();
						selectedDirectorys.add(selected);
						System.out.println(node.getUserObject().toString());

						callListeners(selected);

						/*
						 * TreePath path = e.getPath(); if
						 * (path.getLastPathComponent() instanceof FileTreeNode)
						 * { FileTreeNode node = (FileTreeNode)
						 * path.getLastPathComponent(); if
						 * (!node.hasListChildren()) { JTree tree = (JTree)
						 * e.getSource(); // the selected node is being
						 * populated here.
						 * //node.listChildren((DefaultTreeModel)
						 * tree.getModel());
						 * 
						 * Object selectedpath = node.getUserObject();
						 * System.out.println(selectedpath.toString());
						 * gridviewfiles.setSelectedDirectory(new
						 * File(selectedpath.toString())); } }
						 */
					}
				}));

		// prepare the tree for expanding or collapsing.
		tree.addTreeWillExpandListener(new TreeWillExpandListener() {
			@Override
			public void treeWillCollapse(TreeExpansionEvent event)
					throws ExpandVetoException {
				TreePath path = event.getPath();
				System.out.println(path.toString());
			}

			@Override
			public void treeWillExpand(TreeExpansionEvent event)
					throws ExpandVetoException {
				TreePath path = event.getPath();
				if (path.getLastPathComponent() instanceof FileTreeNode) {
					FileTreeNode node = (FileTreeNode) path
							.getLastPathComponent();
					if (!node.hasListChildren()) {
						JTree tree = (JTree) event.getSource();

						// the selected node is being populated here.
						node.listChildrenDirectoryOnly((DefaultTreeModel) tree
								.getModel());
					}
				}
			}
		});

		// expand the first child of root.
		tree.expandRow(0);
	}

	// Listener
	private LinkedList<ISelectedDirectoryListener> listeners = new LinkedList<>();

	public void addListener(ISelectedDirectoryListener listener) {
		listeners.add(listener);
	}

	public void removeListener(ISelectedDirectoryListener listener) {
		listeners.remove(listener);
	}

	private void callListeners(File selected) {
		for (ISelectedDirectoryListener x : listeners) {
			x.pathChange(selected);
		}
	}

 

}