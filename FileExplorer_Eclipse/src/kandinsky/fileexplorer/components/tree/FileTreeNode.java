package kandinsky.fileexplorer.components.tree;


import java.io.File;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

// custom node for our tree
public class FileTreeNode extends DefaultMutableTreeNode {
    private boolean flag = false;

    public FileTreeNode(File dir)
    {
        super(dir);
    }

    // populate all children ( files and directories ) to the node.
    public void listChildren(DefaultTreeModel model)
    {
        //flag = true;
        File dir = (File) getUserObject();
        FileSystemView fsv = FileSystemView.getFileSystemView();
        File[] children = fsv.getFiles(dir, true);
        for (File child : children) {
            System.out.println(child.toString());
            //model.insertNodeInto(new FileTreeNode(child), this, getChildCount());
        }    
    }

    // populate only the directory to the node.
    public void listChildrenDirectoryOnly(DefaultTreeModel model)
    {
        flag = true;
        File dir = (File) getUserObject();
        FileSystemView fsv = FileSystemView.getFileSystemView();
        File[] children = fsv.getFiles(dir, true);
        for (File child : children) {
            if (child.isDirectory()) {
                model.insertNodeInto(new FileTreeNode(child), this, getChildCount());
            }
        }    
    }

    public boolean hasListChildren()
    {
        return flag ;
    }

    @Override
    public String toString()
    {
        File dir = (File) getUserObject();
        FileSystemView fsv = FileSystemView.getFileSystemView();

        return dir == null ? "" : fsv.getSystemDisplayName(dir);
    }

    @Override
    public boolean isLeaf()
    {
        File dir = (File) getUserObject();
        FileSystemView fsv = FileSystemView.getFileSystemView();

        if (fsv.isFloppyDrive(dir)) {
            return false;
        }
        else {
            return dir.isFile();
        }
    }
}