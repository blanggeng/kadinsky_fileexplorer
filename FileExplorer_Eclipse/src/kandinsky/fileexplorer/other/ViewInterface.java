package kandinsky.fileexplorer.other;

import java.io.File;

public interface ViewInterface {
	
	public abstract File getSelectedDirectory();

}
