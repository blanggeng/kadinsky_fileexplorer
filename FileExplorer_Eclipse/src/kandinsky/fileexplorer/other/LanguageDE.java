package kandinsky.fileexplorer.other;

/**
 * 
 * @author Philip
 * Klasse zum aendern von Texten der Gui 
 *
 */
public class LanguageDE {


	//Tooltips
	public static final String TTSEARCH = "Zum suchen den Suchbegriff in die rechte Textflaeche eingeben";
	public static final String TTLASTUSED = "Zuletzt verwendet";
	public static final String TTNEWFILE = "erstellt einen neuen Ordner";
	public static final String TTNEWDATA = "erstellte eine neue Datei";
	public static final String TTFOR = "Vorw�rts";
	public static final String TTBACK = "Zur�ck";
	public static final String TTADDFAVORITE = "f�ge als Favorit hinzu";
	public static final String TTCOPYTO = "Kopiere nach...";
	public static final String TTFAVORITEN = "Liste alle vom Benutzer gesetzten Favoriten";
	public static final String TTTREE = "Ein Dateibaum";
	public static final String TTSHOWHIDEN = "zeige versteckte Dateien";
	public static final String TTFILEOPTION = "Ordneroptionen";
	public static final String TTADDLEFTWINDOW = "2 Fenstermodus aktivieren";
	public static final String TTHIDETREE = "verstecke Dateibaum";
	
	
	
}
