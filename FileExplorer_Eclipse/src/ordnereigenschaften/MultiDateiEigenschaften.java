package ordnereigenschaften;


import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;

import java.awt.BorderLayout;

import javax.swing.JLabel;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.nio.file.attribute.UserPrincipal;
import java.text.DateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.awt.Dimension;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BoxLayout;

import java.awt.FlowLayout;

import javax.swing.JList;
import javax.swing.border.TitledBorder;

import kandinsky.fileexplorer.components.functions.FileHandler;


/**
 * @author sbattis
 * Ein Dialog der zusammengefasste Informationen mehrerer Dateien anzeigt
 */
public class MultiDateiEigenschaften extends JDialog {

	private static final long serialVersionUID = 1L;
	
	private List<File> files;
	
	/**
	 * Erstellt einen Eigenschaftendialog f�r mehrere Dateien, der mit folgendem Aufruf angezeigt wird: setVisible(true);
	 * @param file
	 */
	public MultiDateiEigenschaften(List<File> files){
		this.files = files;
		
		createDialog();
	}
	
	/**
	 * Sucht die Dateiattribute einer Datei zusammen in der unter anderem steht wann sie erstellt wurde usw
	 * @param file - die Datei f�r die man die Attribute sucht
	 * @return Attribute der Datei
	 */
	private BasicFileAttributes getAttributes(File file){
		BasicFileAttributes attrs;
		
		
		try {
			attrs = Files.readAttributes(file.toPath(), BasicFileAttributes.class);
		} catch (IOException e1) {
			throw new RuntimeException("Kann Dateiattribute nicht lesen!");
		}
		
		return attrs;
	}
	
	/**
	 * Sucht den Besitzer einer Datei und gibt dessen Namen zur�ck
	 * @param file - die Datei f�r den man den Besitzer braucht
	 * @return Name des Besitzers
	 */
	private String getOwnerStr(File file){
		String owner = "";
		
		try {
			UserPrincipal ownerPrinc = Files.getOwner(file.toPath());
			owner = ownerPrinc.getName();
		} catch (IOException e2) {
			owner = "Unbekannt";
		}
		
		return owner;
	}


	
	/**
	 * Erstellt aus einer Dateizeit (Erstelldatum, letzte �nderung usw) einen auf das lokale Zeitformat angepassten String
	 * @param time - die Dateizeit f�r die man die Zeit ausgeben m�chte
	 * @return String im lokalen Zeitformat
	 */
	private String toTimeStr(FileTime time){
		DateFormat format = DateFormat.getDateInstance();
		return format.format(new Date(time.toMillis()));
	}
	

	/**
	 * Ermittelt den Dateityp als String.
	 * Der Typ ist bei regul�ren Dateien der MIME-Typ, bei Verzeichnissen "Verzeichnis" und bei Verkn�pfungen "Verkn�pfung"
	 * @return Dateityp 
	 */
	private String getFileType(File file){
		String result;
		
		try {
			result = Files.probeContentType(file.toPath());
			if(result == null){
				if(file.isDirectory()){
					result = "Verzeichnis";
				} else {
					result = "Verkn\u00FCpfung";
				}
			}
		} catch (IOException e) {
			throw new RuntimeException("Kann Dateityp nicht lesen!");
		}
		return result;
	}
	
	/**
	 * Erstellt einen String aller Nutzer denen mindestens eine der Dateien geh�rt.
	 * Werden durch Komma getrennt
	 * @return String mit allen Nutzern
	 */
	private String getAllOwners(){
		LinkedList<String> owners = new LinkedList<String>();
		String owner;
		for(File f : files){
			owner = getOwnerStr(f);
			if(!owners.contains(owner)){
				owners.add(owner);
			}
		}
		
		String result = "";
		for(String str : owners){
			result += str + ", ";
		}
		if(!result.isEmpty()){
			result = result.substring(0, result.length() - 2);
		}
		
		return result;
	}
	
	/**
	 * Erstellt einen String aus allen Typen, die die Dateien haben.
	 * Jeder Typ kommt nur einmal vor und die Typen werden durch Komma getrennt
	 * @return
	 */
	private String getAllTypes(){
		LinkedList<String> types = new LinkedList<String>();
		String type;
		for(File f : files){
			type = getFileType(f);
			if(!types.contains(type)){
				types.add(type);
			}
		}
		
		String result = "";
		for(String str : types){
			result += str + ", ";
		}
		
		if(!result.isEmpty()){
			result = result.substring(0, result.length() - 2);
		}
		
		return result;
	}
	
	/**
	 * Liest die Gr��e einer Datei aus
	 * @param file - die Datei f�r die die Gr��e gesucht wird
	 * @return die Gr��e in Bytes
	 */
	private long getFileSize(File file){		
		if(file.isDirectory()){
			return 0L; //ggf Ordnergoesse ermitteln
		}
		return file.length();
	}
	
	/**
	 * Z�hlt die Gr��e aller Dateien im Eigenschaftendialog zusammen
	 * @return die Gr��e in Bytes
	 */
	private long getAllFilesSize(){
		long result = 0;
		for(File f : files){
			result += getFileSize(f);
		}
		return result;
	}
	
	/**
	 * Sucht den Zeitraum in dem die Dateien im Eigenschaftendialog ver�ndert wurden.
	 * Dieser Zeitraum wird als String zur�ckgegeben mit dem Format "[von] - [bis]"
	 * @return Zeitraum in Textform
	 */
	private String getModifiedTimes(){
		BasicFileAttributes attrs;
		FileTime min = null;
		FileTime max = null;
		
		for(File f : files){
			attrs = getAttributes(f);
			
			if(min == null || min.toMillis() > attrs.lastModifiedTime().toMillis()){
				min = attrs.lastModifiedTime();
			}
			if(max == null || max.toMillis() < attrs.lastModifiedTime().toMillis()){
				max = attrs.lastModifiedTime();
			}
		}
		
		return toTimeStr(min) + " - " + toTimeStr(max);
	}
	
	/**
	 * K�rzt einen String der zu lang ist (mehr als 40 Zeichen) ab
	 * @param str - der zu pr�fende String
	 * @return der ggf abgek�rzte String
	 */
	private String shortenStr(String str){
		return str.replaceAll("^(.{40}).+$", "$1...");
	}
	
	/**
	 * Sucht den Zeitraum in dem die Dateien im Eigenschaftendialog erstellt wurden.
	 * Dieser Zeitraum wird als String zur�ckgegeben mit dem Format "[von] - [bis]"
	 * @return Zeitraum in Textform
	 */
	private String getCreationTimes(){
		BasicFileAttributes attrs;
		FileTime min = null;
		FileTime max = null;
		
		for(File f : files){
			attrs = getAttributes(f);
			
			if(min == null || min.toMillis() > attrs.creationTime().toMillis()){
				min = attrs.creationTime();
			}
			if(max == null || max.toMillis() < attrs.creationTime().toMillis()){
				max = attrs.creationTime();
			}
		}
		
		return toTimeStr(min) + " - " + toTimeStr(max);
	}
	
	/**
	 * Erstellt aus einer Byteanzahl einen formatierten Text, der auf MB usw hochrechnet
	 * @param bytes - die Anzahl Bytes die formatiert werden sollen
	 * @return der formatierte String
	 */
	private String getSizeString(long bytes){
		long sizeNum;
		String sizeType;
		
		if(bytes < 1024){
			sizeNum = bytes;
			sizeType = " Bytes";
		} else if(bytes < 1024 * 1024) {
			sizeNum = bytes / 1024;
			sizeType = " KB";
		} else if(bytes < 1024 * 1024 * 1024){
			sizeNum = bytes / (1024 * 1024);
			sizeType = " MB";
		} else {
			sizeNum = bytes / (1024 * 1024 * 1024);
			sizeType = " GB";
		}
		
		return "" + sizeNum + sizeType;
	}
	
	
	
	
	
	
	
	
	/**
	 * Erstellt den Dialog mit allen Elementen
	 */
	private void createDialog(){
		setLocationByPlatform(true);
		setModal(true);
		setResizable(false);
		
		setTitle("Eigenschaften");
		setMinimumSize(new Dimension(350, 350));
		setPreferredSize(new Dimension(350, 350));
		setSize(new Dimension(350, 350));
		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.X_AXIS));
		
		JPanel mainPanel = new JPanel();
		getContentPane().add(mainPanel);
		mainPanel.setLayout(new BorderLayout(0, 0));
		
		JPanel northpanel = new JPanel();
		mainPanel.add(northpanel, BorderLayout.NORTH);
		northpanel.setLayout(new BoxLayout(northpanel, BoxLayout.Y_AXIS));
		
		northpanel.add(createDetailsPanel());
		
		mainPanel.add(createFilesList(), BorderLayout.CENTER);
		
		
		mainPanel.add(createButtonPanel(), BorderLayout.SOUTH);
	}
	
	private JList<Object> createFilesList(){
		final JList<Object> filesList = new JList<Object>(files.toArray());
		
		filesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		filesList.setLayoutOrientation(JList.VERTICAL);
		
		filesList.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent evt) {
				if(SwingUtilities.isLeftMouseButton(evt)){
					if(evt.getClickCount() == 2){
						int index = filesList.getSelectedIndex();
						File f = (File)filesList.getModel().getElementAt(index);
						DateiEigenschaften dialog = new DateiEigenschaften(f);
						dialog.setVisible(true);
					}
				}
			}
		});
		
		return filesList;
	}
	
	/**
	 * Erstellt das Panel in dem die zusammengefassten Daten stehen
	 * @return JPanel
	 */
	private JPanel createDetailsPanel(){
		JPanel infoPanel = new JPanel();
		infoPanel.setBorder(null);
		infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.Y_AXIS));
		
		
		JPanel ownerPanel = new JPanel();
		String owners = getAllOwners();
		infoPanel.add(ownerPanel);
		ownerPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		JLabel lblOwners = new JLabel("Besitzer: " + shortenStr(owners));
		lblOwners.setToolTipText(owners);
		ownerPanel.add(lblOwners);
		
		
		JPanel sizePanel = new JPanel();
		infoPanel.add(sizePanel);
		sizePanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		long size = getAllFilesSize();
		String sizeStr = getSizeString(size);
		JLabel lblGre = new JLabel("Gesamtgr\u00F6\u00DFe: " + shortenStr(sizeStr));
		lblGre.setToolTipText(sizeStr);
		sizePanel.add(lblGre);
		
		JPanel fileTypePanel = new JPanel();
		infoPanel.add(fileTypePanel);
		fileTypePanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		String types = getAllTypes();
		JLabel lblDateityp = new JLabel("Dateitypen: " + shortenStr(types));
		lblDateityp.setToolTipText(types);
		fileTypePanel.add(lblDateityp);
		
		JPanel lastAccessPanel = new JPanel();
		infoPanel.add(lastAccessPanel);
		lastAccessPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		String modTimes = getModifiedTimes();
		JLabel lblLetzterZugriff = new JLabel("letzte Zugriffe: " + shortenStr(modTimes));
		lblLetzterZugriff.setToolTipText(modTimes);
		
		lastAccessPanel.add(lblLetzterZugriff);
		
		JPanel CreationDatePanel = new JPanel();
		infoPanel.add(CreationDatePanel);
		CreationDatePanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		String createTimes = getCreationTimes();
		JLabel lblErstelldatum = new JLabel("Erstelldatum: " + shortenStr(createTimes));
		lblErstelldatum.setToolTipText(createTimes);
		CreationDatePanel.add(lblErstelldatum);
		
		JPanel filesPanel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) filesPanel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		infoPanel.add(filesPanel);
		
		return infoPanel;
	}
	
	
	/**
	 * Erstellt das unterste Panel in dem der OK Button liegt
	 * @return Panel
	 */
	private JPanel createButtonPanel(){
		JPanel ButtonPanel = new JPanel();
		
		
		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				
			}
		});
		ButtonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		ButtonPanel.add(btnOk);

		return ButtonPanel;
	}
}

