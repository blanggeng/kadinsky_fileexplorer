package ordnereigenschaften;


import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JTextField;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.nio.file.attribute.UserPrincipal;
import java.text.DateFormat;
import java.util.Date;
import java.awt.Dimension;

import javax.swing.JCheckBox;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.BoxLayout;

import java.awt.FlowLayout;

import javax.swing.JTabbedPane;

import kandinsky.fileexplorer.components.functions.FileHandler;


/**
 * @author sbattis
 * Ein Dialog der Details einer Datei anzeigt und diese Datei l�schen, umbenennen, verstecken und die Rechte dieser Datei setzen kann.
 */
public class DateiEigenschaften extends JDialog {

	private static final long serialVersionUID = 1L;
	
	private File file;
	
	private JTextField nameTextBox;
	private JCheckBox execBox;
	private JCheckBox readBox;
	private JCheckBox writeBox;
	private JCheckBox hiddenBox;
	
	/**
	 * Erstellt einen Dateieigenschaftendialog, der mit folgendem Aufruf angezeigt wird: setVisible(true);
	 * @param file - die Datei f�r die die Eigenschaften gesucht werden sollen
	 */
	public DateiEigenschaften(File file){
		this.file = file;
		
		createDialog();
	}
	
	
	/**
	 * Sucht den Namen des Benutzers der zum Eigenschaftenfenster zugeh�rigen Datei
	 * @return Name des Benutzer als String
	 */
	private String getOwnerStr(){
		String owner = "";
		
		try {
			UserPrincipal ownerPrinc = Files.getOwner(file.toPath());
			owner = ownerPrinc.getName();
		} catch (IOException e2) {
			owner = "Unbekannt";
		}
		
		return owner;
	}
	
	/**
	 * Sucht die Attribute (Erstelldatum, letzter Zugriff, ...) der zum Eigenschaftenfenster zugeh�rigen Datei
	 * @return Attribute der Datei
	 */
	private BasicFileAttributes getAttributes(){
		BasicFileAttributes attrs;
		
		
		try {
			attrs = Files.readAttributes(file.toPath(), BasicFileAttributes.class);
		} catch (IOException e1) {
			throw new RuntimeException("Kann Dateiattribute nicht lesen!");
		}
		
		return attrs;
	}

	
	
	
	/**
	 * Setzt die Dateirechte neu (wenn der Nutzer auf �bernehmen oder Ok klickt)
	 */
	private void updatePermissions(){
		FileHandler.setPermissions(file, readBox.isSelected(), writeBox.isSelected(), execBox.isSelected());
	}
	
	/**
	 * L�scht die Datei komplett (ohne Papierkorb) und fragt den Nutzer vorher nochmal.
	 */
	private void deleteFile(){
		int abfrage = JOptionPane.showConfirmDialog(null, "Die Daten k\u00F6nnen nicht wiederhergestellt werden. Sind Sie sicher?", 
				"L\u00F6chen...", JOptionPane.YES_NO_OPTION);
		if(abfrage == JOptionPane.YES_OPTION){
			file.delete();
		}
	}
	
	private String toTimeStr(FileTime time){
		DateFormat format = DateFormat.getDateTimeInstance();
		return format.format(new Date(time.toMillis()));
	}
	
	
	
	
	/**
	 * Setzt den Versteckstatus neu (wenn der Nutzer auf �bernehmen oder Ok klickt)
	 */
	private void updateHidden(){
		FileHandler.setFileHidden(file, hiddenBox.isSelected());
	}
	
	/**
	 * Speichert die �nderungen (wenn der Nutzer auf �bernehmen oder OK klickt)
	 */
	private void savePreferences(){
		updatePermissions();
		updateHidden();
		
		if(!file.getName().equals(nameTextBox.getText())){
			file.renameTo(new File(file.getParent(), nameTextBox.getText()));
		}
	}
	
	/**
	 * Ermittelt den Dateityp als String.
	 * Der Typ ist bei regul�ren Dateien der MIME-Typ, bei Verzeichnissen "Verzeichnis" und bei Verkn�pfungen "Verkn�pfung"
	 * @return Dateityp 
	 */
	private String getFileType(){
		String result;
		
		try {
			result = Files.probeContentType(file.toPath());
			if(result == null){
				if(file.isDirectory()){
					result = "Verzeichnis";
				} else {
					result = "Verkn\u00FCpfung";
				}
			}
		} catch (IOException e) {
			throw new RuntimeException("Kann Dateityp nicht lesen!");
		}
		return result;
	}
	
	private long getFileSize(){		
		if(file.isDirectory()){
			return 0L; //ggf Ordnergoesse ermitteln
		}
		return file.length();
	}
	
	private String getSizeString(){
		long bytes = getFileSize();
		long sizeNum;
		String sizeType;
		
		if(bytes < 1024){
			sizeNum = bytes;
			sizeType = " Bytes";
		} else if(bytes < 1024 * 1024) {
			sizeNum = bytes / 1024;
			sizeType = " KB";
		} else if(bytes < 1024 * 1024 * 1024){
			sizeNum = bytes / (1024 * 1024);
			sizeType = " MB";
		} else {
			sizeNum = bytes / (1024 * 1024 * 1024);
			sizeType = " GB";
		}
		
		return "" + sizeNum + sizeType;
	}
	
	
	
	
	
	
	
	
	/**
	 * Erstellt den Dialog mit allen Elementen
	 */
	private void createDialog(){
		setLocationByPlatform(true);
		setModal(true);
		setResizable(false);
		
		setTitle(file.getName() + " - Eigenschaften");
		setMinimumSize(new Dimension(300, 350));
		setPreferredSize(new Dimension(300, 350));
		setSize(new Dimension(300, 350));
		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.X_AXIS));
		
		JPanel mainPanel = new JPanel();
		getContentPane().add(mainPanel);
		mainPanel.setLayout(new BorderLayout(0, 0));
		
		JPanel northpanel = new JPanel();
		mainPanel.add(northpanel, BorderLayout.NORTH);
		northpanel.setLayout(new BoxLayout(northpanel, BoxLayout.Y_AXIS));
		
		
		northpanel.add(createNamePanel());
		northpanel.add(createTabbedPane());
		
		mainPanel.add(createButtonPanel(), BorderLayout.SOUTH);
	}
	
	/**
	 * Erstellt das Panel in dem der Dateiname liegt
	 * @return Panel mit dem Namen
	 */
	private JPanel createNamePanel(){
		JPanel namePanel = new JPanel();
		namePanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		JLabel lblName = new JLabel("Name:");
		namePanel.add(lblName);
		
		nameTextBox = new JTextField();
		nameTextBox.setText(file.getName());
		nameTextBox.setColumns(20);
		namePanel.add(nameTextBox);
		
		
		return namePanel;
	}
	
	/**
	 * Erstellt das Panel in dem die Details der Datei stehen wie Erstelldatum, Besitzer, Rechte usw
	 * @return JPanel
	 */
	private JPanel createDetailsPanel(){
		String owner = getOwnerStr();
		BasicFileAttributes attrs = getAttributes();
		
		JPanel infoPanel = new JPanel();
		infoPanel.setBorder(null);
		infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.Y_AXIS));
		
		JPanel pathPanel = new JPanel();
		infoPanel.add(pathPanel);
		pathPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		JLabel lblOrt = new JLabel("Ort: " + file.getParent());
		pathPanel.add(lblOrt);
		
		JPanel sizePanel = new JPanel();
		infoPanel.add(sizePanel);
		sizePanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		JLabel lblGre = new JLabel("Gr\u00F6\u00DFe: " + getSizeString());
		sizePanel.add(lblGre);
		
		JPanel fileTypePanel = new JPanel();
		infoPanel.add(fileTypePanel);
		fileTypePanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		JLabel lblDateityp = new JLabel("Dateityp: " + getFileType());
		fileTypePanel.add(lblDateityp);
		
		JPanel lastAccessPanel = new JPanel();
		infoPanel.add(lastAccessPanel);
		lastAccessPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		JLabel lblLetzterZugriff = new JLabel("letzter Zugriff:" + toTimeStr(attrs.lastModifiedTime()));
		lastAccessPanel.add(lblLetzterZugriff);
		
		JPanel CreationDatePanel = new JPanel();
		infoPanel.add(CreationDatePanel);
		CreationDatePanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		JLabel lblErstelldatum = new JLabel("Erstelldatum: " + toTimeStr(attrs.creationTime()));
		CreationDatePanel.add(lblErstelldatum);
		
		JPanel ownerPanel = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) ownerPanel.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		infoPanel.add(ownerPanel);
		
		JLabel ownerLabel = new JLabel("Besitzer: " + owner);
		ownerPanel.add(ownerLabel);
		
		return infoPanel;
	}
	
	/**
	 * Erstellt das Panel in dem die Einstellungen liegen wie Verstecken, l�schen usw
	 * @return Einstellungspanel
	 */
	private JPanel createSettingsPanel(){
		JPanel CheckboxPanel = new JPanel();
		CheckboxPanel.setBorder(null);
		CheckboxPanel.setLayout(new BoxLayout(CheckboxPanel, BoxLayout.Y_AXIS));
		
		JPanel hiddenPanel = new JPanel();
		CheckboxPanel.add(hiddenPanel);
		hiddenPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		hiddenBox = new JCheckBox("Versteckt");
		hiddenBox.setSelected(file.isHidden());
		hiddenPanel.add(hiddenBox);
		
		JPanel readPanel = new JPanel();
		CheckboxPanel.add(readPanel);
		readPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		readBox = new JCheckBox("Leserecht");
		readBox.setSelected(file.canRead());
		readPanel.add(readBox);
		
		JPanel writePanel = new JPanel();
		CheckboxPanel.add(writePanel);
		writePanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		writeBox = new JCheckBox("Schreibrecht");
		writeBox.setSelected(file.canWrite());
		writePanel.add(writeBox);
		
		JPanel execPanel = new JPanel();
		CheckboxPanel.add(execPanel);
		execPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		execBox = new JCheckBox("Ausf\u00FChrrecht");
		execBox.setSelected(file.canExecute());
		execPanel.add(execBox);
		
		JPanel delPanel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) delPanel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		CheckboxPanel.add(delPanel);
		
		JButton btnLschen = new JButton("L\u00F6schen");
		btnLschen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteFile();
				setVisible(false);
			}
		});
		delPanel.add(btnLschen);
		
		return CheckboxPanel;
	}
	
	/**
	 * Erstellt das TabPanel in dem man zwischen Dateidetails und Einstellungen wechseln kann
	 * @return TabbedPane
	 */
	private JTabbedPane createTabbedPane(){
		JTabbedPane tabPane = new JTabbedPane(JTabbedPane.TOP);
		tabPane.addTab("Details", null, createDetailsPanel(), null);
		tabPane.addTab("Einstellungen", null, createSettingsPanel(), null);
		return tabPane;
	}
	
	
	/**
	 * Erstellt das unterste Panel in dem die Buttons OK, Abbrechen und �bernehmen liegen
	 * @return Panel
	 */
	private JPanel createButtonPanel(){
		JPanel ButtonPanel = new JPanel();
		
		
		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				savePreferences();
				setVisible(false);
				
			}
		});
		ButtonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		ButtonPanel.add(btnOk);
		
		JButton btnAbbrechen = new JButton("Abbrechen");
		btnAbbrechen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		});
		ButtonPanel.add(btnAbbrechen);
		
		JButton btnbernehmen = new JButton("\u00DCbernehmen");
		btnbernehmen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				savePreferences();
			}
		});
		ButtonPanel.add(btnbernehmen);
		
		return ButtonPanel;
	}
}

